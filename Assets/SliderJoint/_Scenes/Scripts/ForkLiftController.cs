﻿namespace Assets.SliderJoint._Scenes.Scripts
{
  using System;

  using Assets.SliderJoint.Scripts;

  using UnityEngine;

  public class ForkLiftController : MonoBehaviour
  {
    [SerializeField]
    private SliderJoint forkJoint;

    [SerializeField]
    private float forkMotorVelocity;

    [SerializeField]
    private float forkMotorForce;

    [SerializeField]
    private float forkMotorBrakeForce;

    [Space]

    [SerializeField]
    private ConfigurableJoint mastJoint;

    [SerializeField]
    private float mastMotorVelocity;

    [SerializeField]
    private float mastMotorForce;

    [SerializeField]
    private float mastMotorBrakeForce;

    private float lastForkInput;

    private float lastMastInput;

    protected void Start()
    {
      Physics.defaultSolverIterations = 60;
      Physics.defaultSolverVelocityIterations = 10;

      this.StopFork();
      this.StopMast();
    }

    protected void FixedUpdate()
    {
      this.ProcessFork();
      this.ProcessMast();
    }

    protected void OnValidate()
    {
      this.forkMotorVelocity = Mathf.Max(0, this.forkMotorVelocity);
      this.forkMotorForce = Mathf.Max(0, this.forkMotorForce);
      this.forkMotorBrakeForce = Mathf.Max(0, this.forkMotorBrakeForce);

      this.mastMotorVelocity = Mathf.Max(0, this.mastMotorVelocity);
      this.mastMotorForce = Mathf.Max(0, this.mastMotorForce);
      this.mastMotorBrakeForce = Mathf.Max(0, this.mastMotorBrakeForce);
    }

    private void ProcessFork()
    {
      var forkInput = Input.GetAxis("Vertical");
      if (Math.Abs(this.lastForkInput - forkInput) > Mathf.Epsilon)
      {
        this.lastForkInput = forkInput;

        if (Math.Abs(forkInput) > Mathf.Epsilon)
        {
          this.MoveFork(forkInput);
        }
        else
        {
          this.StopFork();
        }
      }
    }

    private void MoveFork(float input)
    {
      var motor = this.forkJoint.Motor;
      motor.TargetVelocity = this.forkMotorVelocity * input;
      motor.Force = this.forkMotorForce;
      this.forkJoint.Motor = motor;

      this.forkJoint.UseMotor = true;
      this.forkJoint.UseSpring = false;     
    }

    private void StopFork()
    {
      var spring = this.forkJoint.Spring;
      spring.Spring = this.forkMotorBrakeForce;
      spring.Damper = 0.0f;
      spring.TargetPosition = this.forkJoint.CurrentPosition;
      this.forkJoint.Spring = spring;

      this.forkJoint.UseSpring = true;
      this.forkJoint.UseMotor = false;    
    }

    private void ProcessMast()
    {
      var mastInput = Input.GetAxis("Horizontal");
      if (Math.Abs(this.lastMastInput - mastInput) > Mathf.Epsilon)
      {
        this.lastMastInput = mastInput;

        if (Math.Abs(mastInput) > Mathf.Epsilon)
        {
          this.MoveMast(mastInput);
        }
        else
        {
          this.StopMast();
        }
      }
    }

    private void MoveMast(float input)
    {
      this.mastJoint.targetAngularVelocity = new Vector3(this.mastMotorVelocity * Mathf.Deg2Rad * input, 0.0f, 0.0f);

      var jointDrive = this.mastJoint.angularXDrive;
      jointDrive.positionSpring = 0.0f;
      jointDrive.positionDamper = this.mastMotorForce;
      jointDrive.maximumForce = this.mastMotorForce;
      this.mastJoint.angularXDrive = jointDrive;
    }

    private void StopMast()
    {
      var currentTilt = Vector3.Angle(this.mastJoint.transform.up, this.mastJoint.connectedBody.transform.up)
                              * Mathf.Sign(
                                Vector3.Dot(
                                  this.mastJoint.transform.right,
                                  Vector3.Cross(this.mastJoint.transform.up, this.mastJoint.connectedBody.transform.up)));

      this.mastJoint.targetRotation = Quaternion.Euler(currentTilt, 0.0f, 0.0f);
      this.mastJoint.targetAngularVelocity = Vector3.zero;

      var jointDrive = this.mastJoint.angularXDrive;
      jointDrive.positionSpring = this.mastMotorBrakeForce;
      jointDrive.positionDamper = 0.0f;
      jointDrive.maximumForce = this.mastMotorBrakeForce;
      this.mastJoint.angularXDrive = jointDrive;
    }
  }
}
