﻿namespace Assets.SliderJoint._Scenes.Scripts
{
  using UnityEngine;

  public class RotationController : MonoBehaviour
  {
    [SerializeField]
    [Range(0.0f, 360.0f)]
    private float speed = 45f;

    protected void Update()
    {
      var xAngle = Input.GetAxis("Vertical") * this.speed * Time.deltaTime;
      var yAngle = Input.GetAxis("Horizontal") * this.speed * Time.deltaTime;
      this.transform.Rotate(xAngle, 0.0f, yAngle, Space.World);
    }
  }
}
