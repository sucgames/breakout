﻿namespace Assets.SliderJoint._Scenes.Scripts
{
  using System;

  using Assets.SliderJoint.Scripts;

  using UnityEngine;

  public class SlidingDoorController : MonoBehaviour
  {
    [SerializeField]
    private SliderJoint slidingDoorJoint;

    [SerializeField]
    private float motorVelocity = 1.0f;

    [SerializeField]
    private float motorForce = float.PositiveInfinity;

    [SerializeField]
    private float brakeForce = float.PositiveInfinity;

    private float lastInput;

    protected void Start()
    {
      Physics.defaultSolverIterations = 60;
      Physics.defaultSolverVelocityIterations = 10;

      this.StopSlidingDoor();
    }

    protected void FixedUpdate()
    {
      this.ProcessFork();
    }

    protected void OnValidate()
    {
      this.motorVelocity = Mathf.Max(0, this.motorVelocity);
      this.motorForce = Mathf.Max(0, this.motorForce);
      this.brakeForce = Mathf.Max(0, this.brakeForce);
    }

    private void ProcessFork()
    {
      var input = Input.GetAxis("Horizontal");
      if (Math.Abs(this.lastInput - input) > Mathf.Epsilon)
      {
        this.lastInput = input;

        if (Math.Abs(input) > Mathf.Epsilon)
        {
          this.MoveSlidingDoor(input);
        }
        else
        {
          this.StopSlidingDoor();
        }
      }
    }

    private void MoveSlidingDoor(float input)
    {
      var motor = this.slidingDoorJoint.Motor;
      motor.TargetVelocity = this.motorVelocity * input;
      motor.Force = this.motorForce;
      this.slidingDoorJoint.Motor = motor;

      this.slidingDoorJoint.UseMotor = true;
      this.slidingDoorJoint.UseSpring = false;
    }

    private void StopSlidingDoor()
    {
      var spring = this.slidingDoorJoint.Spring;
      spring.Spring = this.brakeForce;
      spring.Damper = 100.0f;
      spring.TargetPosition = this.slidingDoorJoint.CurrentPosition;
      this.slidingDoorJoint.Spring = spring;

      this.slidingDoorJoint.UseSpring = true;
      this.slidingDoorJoint.UseMotor = false;
    }
  }
}
