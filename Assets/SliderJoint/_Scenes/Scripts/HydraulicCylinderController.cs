﻿namespace Assets.SliderJoint._Scenes.Scripts
{
  using System;

  using Assets.SliderJoint.Scripts;

  using UnityEngine;

  public class HydraulicCylinderController : MonoBehaviour
  {
    [SerializeField]
    private SliderJoint pistonJoint;

    [SerializeField]
    private float motorVelocity = 1.0f;

    [SerializeField]
    private float motorForce = float.PositiveInfinity;

    [SerializeField]
    private float brakeForce = float.PositiveInfinity;

    private float lastInput;

    protected void Start()
    {
      Physics.defaultSolverIterations = 60;
      Physics.defaultSolverVelocityIterations = 10;

      this.StopCylinder();
    }

    protected void FixedUpdate()
    {
      this.ProcessFork();
    }

    protected void OnValidate()
    {
      this.motorVelocity = Mathf.Max(0, this.motorVelocity);
      this.motorForce = Mathf.Max(0, this.motorForce);
      this.brakeForce = Mathf.Max(0, this.brakeForce);
    }

    private void ProcessFork()
    {
      var input = Input.GetAxis("Vertical");
      if (Math.Abs(this.lastInput - input) > Mathf.Epsilon)
      {
        this.lastInput = input;

        if (Math.Abs(input) > Mathf.Epsilon)
        {
          this.MoveCylinder(input);
        }
        else
        {
          this.StopCylinder();
        }
      }
    }

    private void MoveCylinder(float input)
    { 
      var motor = this.pistonJoint.Motor;
      motor.TargetVelocity = this.motorVelocity * input;
      motor.Force = this.motorForce;
      this.pistonJoint.Motor = motor;

      this.pistonJoint.UseMotor = true;
      this.pistonJoint.UseSpring = false;
    }

    private void StopCylinder()
    {
      var spring = this.pistonJoint.Spring;
      spring.Spring = this.brakeForce;
      spring.Damper = 100.0f;
      spring.TargetPosition = this.pistonJoint.CurrentPosition;
      this.pistonJoint.Spring = spring;

      this.pistonJoint.UseSpring = true;
      this.pistonJoint.UseMotor = false;
    }
  }
}
