﻿namespace Assets.SliderJoint.Scripts
{
  using System;

  using UnityEngine;

  /// <summary>
  /// The JointMotor is used by the SliderJoint to motorize a joint.
  /// </summary>
  [Serializable]
  public struct JointMotor
  {
    /// <summary>
    /// The speed the object tries to attain
    /// </summary>
    [Tooltip("The speed the object tries to attain")]
    public float TargetVelocity;

    /// <summary>
    /// The force applied in order to attain the speed
    /// </summary>
    [Tooltip("The force applied in order to attain the speed.")]
    public float Force;
  }
}