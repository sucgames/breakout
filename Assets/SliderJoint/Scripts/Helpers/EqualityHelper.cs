﻿namespace Assets.SliderJoint.Scripts.Helpers
{
  using UnityEngine;

  public static class EqualityHelper
  {
    public const float DefaultThreshold = 0.01f;

    public static bool Equals(float value1, float value2, float threshold = DefaultThreshold)
    {
      return Mathf.Abs(value1 - value2) < threshold;
    }

    public static bool Equals(Vector3 value1, Vector3 value2, float threshold = DefaultThreshold)
    {
      return Equals(value1.x, value2.x) && Equals(value1.y, value2.y) && Equals(value1.z, value2.z);
    }
  }
}
