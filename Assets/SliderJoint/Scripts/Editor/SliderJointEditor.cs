﻿namespace Assets.SliderJoint.Scripts.Editor
{
  using System;
  using System.Collections.Generic;
  using System.IO;
  using System.Linq;

  using UnityEditor;

  using UnityEngine;

  [CustomEditor(typeof(SliderJoint))]
  [CanEditMultipleObjects]
  public class SliderJointEditor : Editor
  {
    private const string IconEditJointLimitsName = "IconEditJointLimits.png";

    private const int SliderSnap = 5;

    private const float MaximumHandleSize = 0.15f;

    private readonly List<SliderJoint> sliderJoints = new List<SliderJoint>();

    private Texture editJointLimitsTexture;

    private bool editJointLimits;

    private Tool previousTool;

    public override void OnInspectorGUI()
    {
      EditorGUILayout.BeginHorizontal(GUILayout.Height(30.0f), GUILayout.MaxWidth(EditorGUIUtility.currentViewWidth));

      GUILayout.FlexibleSpace();

      EditorGUI.BeginChangeCheck();
      this.editJointLimits = GUILayout.Toggle(
        this.editJointLimits,
        this.editJointLimitsTexture,
        "Button",
        GUILayout.Height(25.0f),
        GUILayout.Width(30.0f),
        GUILayout.ExpandWidth(false));

      EditorGUILayout.LabelField(
        "Edit Joint Limits",
        new GUIStyle(EditorStyles.label) { alignment = TextAnchor.MiddleLeft },
        GUILayout.Height(30.0f),
        GUILayout.ExpandWidth(true));

      if (EditorGUI.EndChangeCheck())
      {
        if (this.editJointLimits)
        {
          this.previousTool = Tools.current;
          Tools.current = Tool.None;
        }
        else
        {
          Tools.current = this.previousTool;
        }
      }

      EditorGUILayout.EndHorizontal();

      this.serializedObject.Update();

      Editor.DrawPropertiesExcluding(this.serializedObject, "m_Script");

      this.serializedObject.ApplyModifiedProperties();
    }

    protected void OnSceneGUI()
    {
      if (this.editJointLimits)
      {
        foreach (var sliderJoint in this.sliderJoints)
        {
          this.DrawHandles(sliderJoint);
        }
      }
    }

    protected void OnEnable()
    {
      this.sliderJoints.AddRange(this.targets.Cast<SliderJoint>());

      this.editJointLimitsTexture =
        AssetDatabase.GetCachedIcon(
          Path.Combine(this.GetType().Namespace.Replace('.', Path.DirectorySeparatorChar), IconEditJointLimitsName));
    }

    protected void OnDisable()
    {
      this.sliderJoints.Clear();
      Tools.current = this.previousTool;
    }

    private void DrawHandles(SliderJoint sliderJoint)
    {
      if (sliderJoint.UseLimits)
      {
        var worldConnectedAnchor = sliderJoint.WorldConnectedAnchor;
        var worldAxisRotation = sliderJoint.WorldAxisRotation;
        var localToWorldMatrix = Matrix4x4.TRS(worldConnectedAnchor, worldAxisRotation, Vector3.one).inverse;

        var limits = sliderJoint.Limits;
        var worldAxisDirection = sliderJoint.WorldAxisDirection;
        var lowerLimitPosition = sliderJoint.LowerLimitWorldPosition;
        var upperLimitPosition = sliderJoint.UpperLimitWorldPosition;

        EditorGUI.BeginChangeCheck();

        Handles.color = Color.red;

        lowerLimitPosition = Handles.Slider(
          lowerLimitPosition,
          worldAxisDirection,
          HandleUtility.GetHandleSize(lowerLimitPosition) * MaximumHandleSize,
          Handles.CylinderHandleCap,
          Mathf.Pow(10.0f, -SliderSnap));

        upperLimitPosition = Handles.Slider(
          upperLimitPosition,
          worldAxisDirection,
          HandleUtility.GetHandleSize(upperLimitPosition) * MaximumHandleSize,
          Handles.CylinderHandleCap,
          Mathf.Pow(10.0f, -SliderSnap));

        if (EditorGUI.EndChangeCheck())
        {
          Undo.RecordObject(sliderJoint, "Slider Joint Change");

          limits.Lower = (float)Math.Round(localToWorldMatrix.MultiplyPoint3x4(lowerLimitPosition).z, SliderSnap);
          limits.Upper = (float)Math.Round(localToWorldMatrix.MultiplyPoint3x4(upperLimitPosition).z, SliderSnap);

          sliderJoint.Limits = limits;
        }
      }
    }
  }
}
