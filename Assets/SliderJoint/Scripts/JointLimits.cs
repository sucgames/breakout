﻿namespace Assets.SliderJoint.Scripts
{
  using System;

  using UnityEngine;

  /// <summary>
  /// JointLimits is used by the SliderJoint to limit the joints position
  /// </summary>
  [Serializable]
  public struct JointLimits
  {
    /// <summary>
    /// The distance in world units from the origin to the lower limit. Must be negative
    /// </summary>
    [Tooltip("The distance in world units from the origin to the lower limit. Must be negative")]
    public float Lower;

    /// <summary>
    /// The distance in world units from the origin to the upper limit. Must be positive
    /// </summary>
    [Tooltip("The distance in world units from the origin to the upper limit. Must be positive")]
    public float Upper;

    /// <summary>
    /// Bounce force applied to the object to push it back when it reaches the limit distance
    /// </summary>
    [Tooltip("Bounce force applied to the object to push it back when it reaches the limit distance")]
    public float Bounciness;

    /// <summary>
    /// The minimum distance tolerance (between the joint position and the limit) at which the limit will be enforced. 
    /// A high tolerance makes the limit less likely to be violated when the object is moving fast. However, this will also require the limit to be taken into account by the physics simulation more often and this will tend to reduce performance slightly
    /// </summary>
    [Tooltip("The minimum distance tolerance (between the joint position and the limit) at which the limit will be enforced. A high tolerance makes the limit less likely to be violated when the object is moving fast. However, this will also require the limit to be taken into account by the physics simulation more often and this will tend to reduce performance slightly")]
    public float ContactDistance;
  }
}