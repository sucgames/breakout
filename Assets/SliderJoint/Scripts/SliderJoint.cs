﻿namespace Assets.SliderJoint.Scripts
{
  using System;

  using Assets.SliderJoint.Scripts.Helpers;

  using UnityEngine;

  [RequireComponent(typeof(Rigidbody))]
  public class SliderJoint : MonoBehaviour
  {
    private const float MinimumBreakValue = 0.001f;

    private const float MinimumMassScaleValue = 0.00001f;

#if UNITY_EDITOR
    private static readonly Color ArrowGizmosColor = new Color(234 / 255f, 152 / 255f, 55 / 255f);
#endif

    [SerializeField]
    [Tooltip("The other Rigidbody object to which the joint is connected. You can set this to None to indicate that the joint is attached to a fixed position in space rather than another Rigidbody")]
    private Rigidbody connectedBody;

    [SerializeField]
    [Tooltip("The local point where the center of the joint is defined. All physics-based simulation will use this point as the center in calculations")]
    private Vector3 anchor = Vector3.zero;

    [SerializeField]
    [Tooltip("The direction of the axis in which the body moves linearly. The axis is defined in local space")]
    private Vector3 axis = Vector3.forward;

    [Space]

    [SerializeField]
    [Tooltip("The spring makes the Rigidbody reach for a specific position compared to its connected body, moving linearly along its defined local axis")]
    private bool useSpring;

    [SerializeField]
    [Tooltip("Properties of the Spring that are used if Use Spring is enabled")]
    private JointSpring spring;

    [Space]

    [SerializeField]
    [Tooltip("The motor makes the object move linearly along its defined local axis")]
    private bool useMotor;

    [SerializeField]
    [Tooltip("Properties of the Motor that are used if Use Motor is enabled")]
    private JointMotor motor;

    [Space]

    [SerializeField]
    [Tooltip("If enabled, the joint’s linear movement will be restricted within the Lower & Upper values, specified as a distance from the joint’s origin")]
    private bool useLimits;

    [SerializeField]
    [Tooltip("Properties of the Limits that are used if Use Limits is enabled")]
    private JointLimits limits;

    [Space]

    [SerializeField]
    [Tooltip("The force that needs to be applied for this joint to break. If the joint is pushed beyond its constraints by a force larger than this value then the joint will be permanently 'broken' and deleted")]
    private float breakForce = float.PositiveInfinity;

    [SerializeField]
    [Tooltip("The torque that needs to be applied for this joint to break. If the joint is rotated beyond its constraints by a torque larger than this value then the joint will be permanently 'broken' and deleted")]
    private float breakTorque = float.PositiveInfinity;

    [SerializeField]
    [Tooltip("Should the object with the joint be able to collide with the connected object (as opposed to just passing through each other)?")]
    private bool enableCollision;

    [SerializeField]
    [Tooltip("If preprocessing is disabled then certain 'impossible' configurations of the joint will be kept more stable rather than drifting wildly out of control")]
    private bool enablePreprocessing = true;

    [SerializeField]
    [Tooltip("The scale to apply to the inverse mass and inertia tensor of the body prior to solving the constraints")]
    private float massScale = 1.0f;

    [SerializeField]
    [Tooltip("The scale to apply to the inverse mass and inertia tensor of the connected body prior to solving the constraints")]
    private float connectedMassScale = 1.0f;

    private ConfigurableJoint configurableJoint;

    private Vector3 connectedAnchor;

    private Vector3 lowerPosition;

    private Vector3 upperPosition;

    private Vector3 jointDirection;

    private JointDrive jointDrive;

    private SoftJointLimit linearLimit;

    private Vector3 targetPosition;

    private Vector3 targetVelocity;

    /// <summary>
    /// The other Rigidbody object to which the joint is connected. 
    /// You can set this to None to indicate that the joint is attached to a fixed position in space rather than another Rigidbody
    /// </summary>
    public Rigidbody ConnectedBody
    {
      get
      {
        return this.connectedBody;
      }

      set
      {
        this.connectedBody = value;

        if (this.configurableJoint != null)
        {
          this.ConfigureJoint(false);
        }
      }
    }

    /// <summary>
    /// The local point where the center of the joint is defined. 
    /// All physics-based simulation will use this point as the center in calculations
    /// </summary>
    public Vector3 Anchor
    {
      get
      {
        return this.anchor;
      }

      set
      {
        this.anchor = value;

        if (this.configurableJoint != null)
        {
          this.ConfigureJoint(false);
        }
      }
    }

    /// <summary>
    /// The direction of the axis in which the body moves linearly. 
    /// The axis is defined in local space
    /// </summary>
    public Vector3 Axis
    {
      get
      {
        return this.axis;
      }

      set
      {
        this.axis = value;

        if (this.configurableJoint != null)
        {
          this.ConfigureJoint(false);
        }
      }
    }

    /// <summary>
    /// The spring makes the Rigidbody reach for a specific position compared to its connected body, moving linearly along its defined local axis
    /// </summary>
    public bool UseSpring
    {
      get
      {
        return this.useSpring;
      }

      set
      {
        this.useSpring = value;

        if (this.configurableJoint != null)
        {
          this.ConfigureJoint(false);
        }
      }
    }

    /// <summary>
    /// Properties of the Spring that are used if Use Spring is enabled
    /// </summary>
    public JointSpring Spring
    {
      get
      {
        return this.spring;
      }

      set
      {
        this.spring = value;
        this.ValidateJointSpring();

        if (this.configurableJoint != null)
        {
          this.ConfigureJoint(false);
        }
      }
    }

    /// <summary>
    /// The motor makes the object move linearly along its defined local axis
    /// </summary>
    public bool UseMotor
    {
      get
      {
        return this.useMotor;
      }

      set
      {
        this.useMotor = value;

        if (this.configurableJoint != null)
        {
          this.ConfigureJoint(false);
        }
      }
    }

    /// <summary>
    /// Properties of the Motor that are used if Use Motor is enabled
    /// </summary>
    public JointMotor Motor
    {
      get
      {
        return this.motor;
      }

      set
      {
        this.motor = value;
        this.ValidateJointMotor();

        if (this.configurableJoint != null)
        {
          this.ConfigureJoint(false);
        }
      }
    }

    /// <summary>
    /// If enabled, the joint’s linear movement will be restricted within the Lower & Upper values, specified as a distance from the joint’s origin
    /// </summary>
    public bool UseLimits
    {
      get
      {
        return this.useLimits;
      }

      set
      {
        this.useLimits = value;

        if (this.configurableJoint != null)
        {
          this.ConfigureJoint(false);
        }
      }
    }

    /// <summary>
    /// Properties of the Limits that are used if Use Limits is enabled
    /// </summary>
    public JointLimits Limits
    {
      get
      {
        return this.limits;
      }

      set
      {
        this.limits = value;
        this.ValidateJointLimits();

        if (this.configurableJoint != null)
        {
          this.ConfigureJoint(false);
        }
      }
    }

    /// <summary>
    /// The force that needs to be applied for this joint to break. 
    /// If the joint is pushed beyond its constraints by a force larger than this value then the joint will be permanently 'broken' and deleted
    /// </summary>
    public float BreakForce
    {
      get
      {
        return this.breakForce;
      }

      set
      {
        this.breakForce = Mathf.Min(MinimumBreakValue, value);

        if (this.configurableJoint != null)
        {
          this.configurableJoint.breakForce = this.breakForce;
        }
      }
    }

    /// <summary>
    /// The torque that needs to be applied for this joint to break. 
    /// If the joint is rotated beyond its constraints by a torque larger than this value then the joint will be permanently 'broken' and deleted
    /// </summary>
    public float BreakTorque
    {
      get
      {
        return this.breakTorque;
      }

      set
      {
        this.breakTorque = Mathf.Min(MinimumBreakValue, value);

        if (this.configurableJoint != null)
        {
          this.configurableJoint.breakTorque = this.breakTorque;
        }
      }
    }

    /// <summary>
    /// Should the object with the joint be able to collide with the connected object (as opposed to just passing through each other)?
    /// </summary>
    public bool EnableCollision
    {
      get
      {
        return this.enableCollision;
      }

      set
      {
        this.enableCollision = value;

        if (this.configurableJoint != null)
        {
          this.configurableJoint.enableCollision = this.enableCollision;
        }
      }
    }

    /// <summary>
    /// If preprocessing is disabled then certain 'impossible' configurations of the joint will be kept more stable rather than drifting wildly out of control
    /// </summary>
    public bool EnablePreprocessing
    {
      get
      {
        return this.enablePreprocessing;
      }

      set
      {
        this.enablePreprocessing = value;

        if (this.configurableJoint != null)
        {
          this.configurableJoint.enablePreprocessing = this.enablePreprocessing;
        }
      }
    }

    /// <summary>
    /// The scale to apply to the inverse mass and inertia tensor of the body prior to solving the constraints
    /// </summary>
    public float MassScale
    {
      get
      {
        return this.massScale;
      }

      set
      {
        this.massScale = Mathf.Max(MinimumMassScaleValue, value);

        if (this.configurableJoint != null)
        {
          this.configurableJoint.massScale = this.massScale;
        }
      }
    }

    /// <summary>
    /// The scale to apply to the inverse mass and inertia tensor of the connected body prior to solving the constraints
    /// </summary>
    public float ConnectedMassScale
    {
      get
      {
        return this.connectedMassScale;
      }

      set
      {
        this.connectedMassScale = Mathf.Max(MinimumMassScaleValue, value);

        if (this.configurableJoint != null)
        {
          this.configurableJoint.connectedMassScale = this.connectedMassScale;
        }
      }
    }

    /// <summary>
    /// The connected anchor position. 
    /// If the joint is connected to another Rigidbody, then the connected anchor position is local to the connected body. 
    /// Otherwise, the connected anchor position is local to this body.
    /// </summary>
    public Vector3 ConnectedAnchor
    {
      get
      {
        return this.connectedAnchor;
      }
    }

    /// <summary>
    /// The current connected anchor world position.
    /// If the joint is attached to a fixed position in space rather than another Rigidbody, then the connected anchor world position is equals to the connected anchor local position
    /// </summary>
    public Vector3 WorldConnectedAnchor
    {
      get
      {
        if (Application.isPlaying)
        {
          if (this.connectedBody != null)
          {
            return this.connectedBody.transform.TransformPoint(this.connectedAnchor);
          }

          return this.connectedAnchor;
        }

        return this.WorldAnchor;
      }
    }

    /// <summary>
    /// The world point where the center of the joint is defined. All physics-based simulation will use this point as the center in calculations
    /// </summary>
    public Vector3 WorldAnchor
    {
      get
      {
        return this.transform.TransformPoint(this.anchor);
      }
    }

    /// <summary>
    /// The world rotation of the axis in which the body moves linearly
    /// </summary>
    public Quaternion WorldAxisRotation
    {
      get
      {
        if (Math.Abs(this.axis.magnitude) >= Mathf.Epsilon)
        {
          return Quaternion.LookRotation(this.transform.TransformDirection(this.axis));
        }

        return this.transform.rotation;
      }
    }

    /// <summary>
    /// The world direction of the axis in which the body moves linearly
    /// </summary>
    public Vector3 WorldAxisDirection
    {
      get
      {
        if (Math.Abs(this.axis.magnitude) >= Mathf.Epsilon)
        {
          return this.transform.TransformDirection(this.axis);
        }

        return this.transform.forward;
      }
    }

    /// <summary>
    /// The world position of the lower limit
    /// </summary>
    public Vector3 LowerLimitWorldPosition
    {
      get
      {
        if (Application.isPlaying)
        {
          if (this.connectedBody != null)
          {
            return this.connectedBody.transform.TransformPoint(this.lowerPosition);
          }

          return this.lowerPosition;
        }

        var scale = this.transform.lossyScale;
        var direction = this.axis.normalized;
        direction.x /= scale.x;
        direction.y /= scale.y;
        direction.z /= scale.z;

        return this.transform.TransformPoint(this.anchor + (direction * this.limits.Lower));
      }
    }

    /// <summary>
    /// The world position of the upper limit
    /// </summary>
    public Vector3 UpperLimitWorldPosition
    {
      get
      {
        if (Application.isPlaying)
        {
          if (this.connectedBody != null)
          {
            return this.connectedBody.transform.TransformPoint(this.upperPosition);
          }

          return this.upperPosition;
        }

        var scale = this.transform.lossyScale;
        var direction = this.axis.normalized;
        direction.x /= scale.x;
        direction.y /= scale.y;
        direction.z /= scale.z;

        return this.transform.TransformPoint(this.anchor + (direction * this.limits.Upper));
      }
    }

    /// <summary>
    /// The current position the joint with respect to connected anchor
    /// </summary>
    public float CurrentPosition
    {
      get
      {
        return Matrix4x4.TRS(this.WorldConnectedAnchor, this.WorldAxisRotation, Vector3.one).inverse.MultiplyPoint3x4(this.WorldAnchor).z;
      }
    }

    protected void Awake()
    {
      this.configurableJoint = this.gameObject.AddComponent<ConfigurableJoint>();
      this.configurableJoint.hideFlags = HideFlags.HideAndDontSave | HideFlags.HideInInspector;

      this.jointDrive = new JointDrive();
      this.linearLimit = new SoftJointLimit();

      this.ConfigureJoint(true);
    }

    protected void OnDestroy()
    {
      if (this.configurableJoint != null)
      {
        Component.Destroy(this.configurableJoint);
        this.configurableJoint = null;
      }
    }

    protected void OnValidate()
    {
      this.breakForce = Mathf.Max(MinimumBreakValue, this.breakForce);
      this.breakTorque = Mathf.Max(MinimumBreakValue, this.breakTorque);

      this.massScale = Mathf.Max(MinimumMassScaleValue, this.massScale);
      this.connectedMassScale = Mathf.Max(MinimumMassScaleValue, this.connectedMassScale);

      this.ValidateJointSpring();
      this.ValidateJointMotor();
      this.ValidateJointLimits();

      if (Application.isPlaying && this.configurableJoint != null)
      {
        this.ConfigureJoint(false);
      }
    }

#if UNITY_EDITOR
    protected void OnDrawGizmosSelected()
    {
      if (this.useLimits)
      {
        var lowerLimitPosition = this.LowerLimitWorldPosition;
        var upperLimitPosition = this.UpperLimitWorldPosition;

        UnityEditor.Handles.color = Color.red;
        UnityEditor.Handles.DrawLine(lowerLimitPosition, upperLimitPosition);

        UnityEditor.Handles.SphereHandleCap(
          0,
          lowerLimitPosition,
          Quaternion.identity,
          UnityEditor.HandleUtility.GetHandleSize(lowerLimitPosition) * 0.075f,
          EventType.Repaint);

        UnityEditor.Handles.SphereHandleCap(
          0,
          upperLimitPosition,
          Quaternion.identity,
          UnityEditor.HandleUtility.GetHandleSize(upperLimitPosition) * 0.075f,
          EventType.Repaint);
      }

      var anchorPosition = this.WorldAnchor;
      var rotation = this.WorldAxisRotation;

      UnityEditor.Handles.color = ArrowGizmosColor;
      UnityEditor.Handles.ArrowHandleCap(
        0,
        anchorPosition,
        rotation,
        UnityEditor.HandleUtility.GetHandleSize(anchorPosition) * 0.5f,
        EventType.Repaint);

      UnityEditor.Handles.color = Color.black;
      UnityEditor.Handles.SphereHandleCap(
        0,
        anchorPosition,
        rotation,
        UnityEditor.HandleUtility.GetHandleSize(anchorPosition) * 0.075f,
        EventType.Repaint);

      if (Application.isPlaying)
      {
        var connectedAnchorPosition = this.WorldConnectedAnchor;

        UnityEditor.Handles.color = ArrowGizmosColor;
        UnityEditor.Handles.ArrowHandleCap(
          0,
          connectedAnchorPosition,
          rotation,
          UnityEditor.HandleUtility.GetHandleSize(connectedAnchorPosition) * 0.5f,
          EventType.Repaint);

        UnityEditor.Handles.color = Color.black;
        UnityEditor.Handles.SphereHandleCap(
          0,
          connectedAnchorPosition,
          rotation,
          UnityEditor.HandleUtility.GetHandleSize(connectedAnchorPosition) * 0.075f,
          EventType.Repaint);
      }
    }
#endif

    private void ConfigureJoint(bool initialState)
    {
      if (initialState)
      {
        this.configurableJoint.autoConfigureConnectedAnchor = false;
        this.configurableJoint.yMotion = ConfigurableJointMotion.Locked;
        this.configurableJoint.zMotion = ConfigurableJointMotion.Locked;
        this.configurableJoint.angularXMotion = ConfigurableJointMotion.Locked;
        this.configurableJoint.angularYMotion = ConfigurableJointMotion.Locked;
        this.configurableJoint.angularZMotion = ConfigurableJointMotion.Locked;

        this.configurableJoint.anchor = this.anchor;

        if (Math.Abs(this.axis.magnitude) < Mathf.Epsilon)
        {
          this.configurableJoint.axis = -Vector3.forward;
        }
        else
        {
          this.configurableJoint.axis = -this.axis;
        }

        this.configurableJoint.connectedBody = this.connectedBody;
        if (this.connectedBody != null)
        {
          this.connectedAnchor = this.connectedBody.transform.InverseTransformPoint(this.transform.TransformPoint(this.anchor));
          this.jointDirection = this.connectedBody.transform.InverseTransformDirection(this.transform.TransformDirection(this.axis.normalized));
        }
        else
        {
          this.connectedAnchor = this.transform.TransformPoint(this.anchor);
          this.jointDirection = this.transform.TransformDirection(this.axis.normalized);
        }
      }
      else
      {
        if (this.connectedBody != this.configurableJoint.connectedBody)
        {
          if (this.configurableJoint.connectedBody == null)
          {
            this.connectedAnchor = this.connectedBody.transform.InverseTransformPoint(this.connectedAnchor);
            this.jointDirection = this.connectedBody.transform.InverseTransformDirection(this.jointDirection);
          }
          else if (this.connectedBody == null)
          {
            this.connectedAnchor = this.configurableJoint.connectedBody.transform.TransformPoint(this.connectedAnchor);
            this.jointDirection = this.configurableJoint.connectedBody.transform.TransformDirection(this.jointDirection);
          }

          this.configurableJoint.connectedBody = this.connectedBody;
        }

        if (!EqualityHelper.Equals(this.configurableJoint.anchor, this.anchor))
        {
          this.configurableJoint.anchor = this.anchor;
        }

        var axisJoint = -this.axis;
        if (Math.Abs(axisJoint.magnitude) < Mathf.Epsilon)
        {
          axisJoint = -Vector3.forward;
        }

        if (!EqualityHelper.Equals(this.configurableJoint.axis, axisJoint))
        {
          this.configurableJoint.axis = axisJoint;
          if (this.connectedBody != null)
          {
            this.jointDirection = this.connectedBody.transform.InverseTransformDirection(this.transform.TransformDirection(this.axis.normalized));
          }
          else
          {
            this.jointDirection = this.transform.TransformDirection(this.axis.normalized);
          }
        }
      }

      this.configurableJoint.breakForce = this.breakForce;
      this.configurableJoint.breakTorque = this.breakTorque;

      this.configurableJoint.enableCollision = this.enableCollision;
      this.configurableJoint.enablePreprocessing = this.enablePreprocessing;

      this.configurableJoint.massScale = this.massScale;
      this.configurableJoint.connectedMassScale = this.connectedMassScale;

      this.linearLimit.bounciness = this.limits.Bounciness;
      this.linearLimit.contactDistance = this.limits.ContactDistance;

      Vector3 jointConnectedAnchor = this.connectedAnchor;
      if (this.useLimits)
      {
        this.linearLimit.limit = (this.limits.Upper - this.limits.Lower) * 0.5f;
        this.configurableJoint.xMotion = ConfigurableJointMotion.Limited;

        if (this.connectedBody != null)
        {
          var scale = this.connectedBody.transform.lossyScale;
          var direction = this.jointDirection;
          direction.x /= scale.x;
          direction.y /= scale.y;
          direction.z /= scale.z;

          this.lowerPosition = this.connectedAnchor + (direction * this.limits.Lower);
          this.upperPosition = this.connectedAnchor + (direction * this.limits.Upper);

          jointConnectedAnchor = (this.upperPosition + this.lowerPosition) * 0.5f;
        }
        else
        {
          this.lowerPosition = this.connectedAnchor + (this.jointDirection * this.limits.Lower);
          this.upperPosition = this.connectedAnchor + (this.jointDirection * this.limits.Upper);

          jointConnectedAnchor = (this.upperPosition + this.lowerPosition) * 0.5f;
        }
      }
      else
      {
        this.linearLimit.limit = 0.0f;
        this.configurableJoint.xMotion = ConfigurableJointMotion.Free;

        this.lowerPosition = Vector3.zero;
        this.upperPosition = Vector3.zero;
      }

      if (initialState || !EqualityHelper.Equals(this.configurableJoint.connectedAnchor, jointConnectedAnchor))
      {
        this.configurableJoint.connectedAnchor = jointConnectedAnchor;
      }

      this.configurableJoint.linearLimit = this.linearLimit;

      if (this.useMotor)
      {
        this.targetVelocity.x = this.motor.TargetVelocity;
        this.jointDrive.positionSpring = 0.0f;
        this.jointDrive.positionDamper = this.motor.Force;
        this.jointDrive.maximumForce = this.motor.Force;
      }
      else
      {
        this.targetVelocity.x = 0.0f;
        this.jointDrive.positionSpring = 0.0f;
        this.jointDrive.positionDamper = 0.0f;
        this.jointDrive.maximumForce = 0.0f;
      }

      if (this.useSpring)
      {
        if (this.useLimits)
        {
          this.targetPosition.x = this.spring.TargetPosition - (this.limits.Upper - this.linearLimit.limit);
        }
        else
        {
          this.targetPosition.x = this.spring.TargetPosition;
        }

        this.jointDrive.positionSpring = Mathf.Max(this.jointDrive.positionSpring, this.spring.Spring);
        this.jointDrive.positionDamper = Mathf.Max(this.jointDrive.positionDamper, this.spring.Damper);
        this.jointDrive.maximumForce = this.jointDrive.positionSpring;
      }
      else
      {
        this.targetPosition.x = 0.0f;
      }

      this.configurableJoint.targetVelocity = this.targetVelocity;
      this.configurableJoint.targetPosition = this.targetPosition;
      this.configurableJoint.xDrive = this.jointDrive;
    }

    private void ValidateJointSpring()
    {
      this.spring.Spring = Mathf.Max(0, this.spring.Spring);
      this.spring.Damper = Mathf.Max(0, this.spring.Damper);
    }

    private void ValidateJointMotor()
    {
      this.motor.Force = Mathf.Max(0, this.motor.Force);
    }

    private void ValidateJointLimits()
    {
      this.limits.Lower = Mathf.Min(this.limits.Lower, 0);
      this.limits.Upper = Mathf.Max(this.limits.Upper, 0);
      this.limits.Bounciness = Mathf.Clamp01(this.limits.Bounciness);
      this.limits.ContactDistance = Mathf.Max(0, this.limits.ContactDistance);
    }
  }
}
