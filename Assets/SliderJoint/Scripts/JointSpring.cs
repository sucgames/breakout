﻿namespace Assets.SliderJoint.Scripts
{
  using System;

  using UnityEngine;

  /// <summary>
  /// JointSpring is used to add a spring force to SliderJoint
  /// </summary>
  [Serializable]
  public struct JointSpring
  {
    /// <summary>
    /// The force the object asserts to move into the position
    /// </summary>
    [Tooltip("The force the object asserts to move into the position")]
    public float Spring;

    /// <summary>
    /// The damper force uses to dampen the spring. The higher this value, the more the object will slow down
    /// </summary>
    [Tooltip("The damper force uses to dampen the spring. The higher this value, the more the object will slow down.")]
    public float Damper;

    /// <summary>
    /// The target position the joint attempts to reach
    /// </summary>
    [Tooltip("The target position the joint attempts to reach")]
    public float TargetPosition;
  }
}