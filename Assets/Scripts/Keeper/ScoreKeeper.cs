using UnityEngine;
public class ScoreKeeper : MonoBehaviour
{
    private int _score = 0;
    [SerializeField]
    private float _comboDuration;
    private bool _isInCombo;

    [Header("現在のコンボ数")]
    [SerializeField]
    private int _comboCount;

    [Header("コンボ時間を入力")]
    [SerializeField]
    private float maxComboTime = 0.5f;

    public void Start()
    {
        _comboDuration = maxComboTime;

        //ハイスコアをアタッチ
        transform.Find("HighScoreText").gameObject.AddComponent<HighScoreManager>();
    }

    public void Update()
    {
        //コンボ中じゃないときはそのまま終了
        if (!_isInCombo) {
            return;
        }
        // 時間を引く

        _comboDuration -= Time.deltaTime;
        if (_comboDuration< 0)
        {
            _isInCombo = false;
            //時間リセット
            _comboDuration = maxComboTime;
        }
    }

    /// <summary>
    /// Enemyが死んだときに実行
    /// </summary>
    public void SendEnemyDeath(GameObject enemyobj, EnemyListEnum enemy,int score)
    {
        //コンボ加算処理
        CountCombo();

        //Enemy送信処理
        int sendCombo = GetCombo();
        int sendScore = GetScore(score , sendCombo); 

        switch (enemy)
        {
            case EnemyListEnum.Enemy_A:
                enemyobj.GetComponent<Enemy_A>().ScoreDisply(sendScore);
                enemyobj.GetComponent<Enemy_A>().ComboDisply(sendCombo);
                break;

            case EnemyListEnum.Enemy_B:
                enemyobj.GetComponent<Enemy_B>().ScoreDisply(sendScore);
                enemyobj.GetComponent<Enemy_B>().ComboDisply(sendCombo);
                break;

            case EnemyListEnum.Enemy_C:
                enemyobj.GetComponent<Enemy_C>().ScoreDisply(sendScore);
                enemyobj.GetComponent<Enemy_C>().ComboDisply(sendCombo);
                break;

            case EnemyListEnum.Enemy_D:
                enemyobj.GetComponent<Enemy_D>().ScoreDisply(sendScore);
                enemyobj.GetComponent<Enemy_D>().ComboDisply(sendCombo);
                break;

            case EnemyListEnum.Enemy_E:
                enemyobj.GetComponent<Enemy_E>().ScoreDisply(sendScore);
                enemyobj.GetComponent<Enemy_E>().ComboDisply(sendCombo);
                break;
        }      
    }

    void CountCombo()
    {
        if (_isInCombo)
        {
            _comboCount++;
            _comboDuration = maxComboTime;
        }
        else
        {
            _isInCombo = true;
            _comboCount = 1;
        }
    }

    int GetCombo()
    {
        return _comboCount;
    }
    int GetScore(int score,int combo)
    {
        return score * combo;
    }
}