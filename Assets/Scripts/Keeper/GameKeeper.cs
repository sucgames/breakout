﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameKeeper : MonoBehaviour {

	private GameObject _soundKeeper;
	public GameObject waveManager;
	public GameObject postProcess;
	public GameObject multiballGenerator;
	public GameObject gameOverText;
	public GameObject cameraController;

	public int BallNumber;

	public int EnemyNumber;
	public int EnemyANumber;
	public int EnemyBNumber;
	public int EnemyCNumber;
	public int EnemyDNumber;
	public int EnemyENumber;

	public bool waveDoneFlag;

	public bool postProcessFlag;

	public bool player1Flag;
	public bool player2Flag;
	public bool player3Flag;
	public bool player4Flag;
	public bool gameOverFlag;

    void Awake() {

		Time.timeScale = 1.0f;

		GameObject ScoreKeeper = GameObject.Find("ScoreKeeper");

        ScoreKeeper.AddComponent<ScoreKeeper>();
        ScoreKeeper.AddComponent<ScoreController>();
		
		_soundKeeper = GameObject.Find("SoundKeeper");

    }

    void Start() {

		postProcessFlag = true;

		gameOverFlag = false;

		waveManager.SendMessage("WaveStart");

        _soundKeeper.GetComponent<SoundKeeper>().PlayWaveStartSound();

    }




    void Update() {

		if ( Input.GetButtonDown ("Cancel") ) {

			GameQuit();

		}




		if ( Input.GetKeyDown ("r") ) {

			Restart();

		}




		if ( Input.GetButtonDown ("Submit") && gameOverFlag == true ) {

			Restart();

		}




		if ( Input.GetKeyDown ("p") ) {

			if ( postProcessFlag == true ) {

				postProcess.gameObject.SetActive(false);
				postProcessFlag = false;

			} else if ( postProcessFlag == false ) {

				postProcess.gameObject.SetActive(true);
				postProcessFlag = true;

			}

		}
		        
    }




	void WaveStart() {

		waveDoneFlag = false;

	}




	void WaveDone() {

		waveDoneFlag = true;

		WaveJudge();

	}




	void Player1_Spawn() {

		player1Flag = true;

	}

	void Player2_Spawn() {

		player2Flag = true;

	}

	void Player3_Spawn() {

		player3Flag = true;

	}

	void Player4_Spawn() {

		player4Flag = true;

	}




	void Player1_Destroy() {

		player1Flag = false;

		GameOverJudge();

	}

	void Player2_Destroy() {

		player2Flag = false;

		GameOverJudge();

	}

	void Player3_Destroy() {

		player3Flag = false;

		GameOverJudge();

	}

	void Player4_Destroy() {

		player4Flag = false;

		GameOverJudge();

	}




	void Ball_Spawn() {

		BallNumber = BallNumber + 1;

        _soundKeeper.GetComponent<SoundKeeper>().PlaySpawnBallSound();
	}

    public void Bullet_Shot() {
        _soundKeeper.GetComponent<SoundKeeper>().PlayBulletShotSound();
    }

    public void Ball_Collide() {
        _soundKeeper.GetComponent<SoundKeeper>().PlayBallCollideSound();
    }



	void Ball_Delete() {

		BallNumber = BallNumber - 1;

		GameOverJudge();

	}




	void EnemyA_Spawn() {

		EnemyNumber = EnemyNumber + 1;

		EnemyANumber = EnemyANumber + 1;
	}

	void EnemyB_Spawn() {

		EnemyNumber = EnemyNumber + 1;

		EnemyBNumber = EnemyBNumber + 1;

	}

	void EnemyC_Spawn() {

		EnemyNumber = EnemyNumber + 1;

		EnemyCNumber = EnemyCNumber + 1;

	}

	void EnemyD_Spawn() {

		EnemyNumber = EnemyNumber + 1;

		EnemyDNumber = EnemyDNumber + 1;

	}

	void EnemyE_Spawn() {

		EnemyNumber = EnemyNumber + 1;

		EnemyENumber = EnemyENumber + 1;

	}




	void EnemyA_Destroy() {

		EnemyNumber = EnemyNumber - 1;

		EnemyANumber = EnemyANumber - 1;

		WaveJudge();

        _soundKeeper.GetComponent<SoundKeeper>().PlayEnemyDestroySound();
	}

	void EnemyB_Destroy() {

		EnemyNumber = EnemyNumber - 1;

		EnemyBNumber = EnemyBNumber - 1;

		WaveJudge();

        _soundKeeper.GetComponent<SoundKeeper>().PlayEnemyDestroySound();

	}

	void EnemyC_Destroy() {

		EnemyNumber = EnemyNumber - 1;

		EnemyCNumber = EnemyCNumber - 1;

		WaveJudge();

        _soundKeeper.GetComponent<SoundKeeper>().PlayEnemyDestroySound();

	}

	void EnemyD_Destroy() {

		EnemyNumber = EnemyNumber - 1;

		EnemyDNumber = EnemyDNumber - 1;

		WaveJudge();

        _soundKeeper.GetComponent<SoundKeeper>().PlayEnemyDestroySound();

	}

	void EnemyE_Destroy() {

		EnemyNumber = EnemyNumber - 1;

		EnemyENumber = EnemyENumber - 1;

		cameraController.SendMessage("CameraShake");

        _soundKeeper.GetComponent<SoundKeeper>().PlayEnemyDestroySound();

		WaveJudge();

	}




	void EnemyA_Delete() {

		EnemyNumber = EnemyNumber - 1;

		EnemyANumber = EnemyANumber - 1;

		WaveJudge();

	}

	void EnemyB_Delete() {

		EnemyNumber = EnemyNumber - 1;

		EnemyBNumber = EnemyBNumber - 1;

		WaveJudge();

	}

	void EnemyC_Delete() {

		EnemyNumber = EnemyNumber - 1;

		EnemyCNumber = EnemyCNumber - 1;

		WaveJudge();

	}

	void EnemyD_Delete() {

		EnemyNumber = EnemyNumber - 1;

		EnemyDNumber = EnemyDNumber - 1;

		WaveJudge();

	}

	void EnemyE_Delete() {

		EnemyNumber = EnemyNumber - 1;

		EnemyENumber = EnemyENumber - 1;

		WaveJudge();

	}




	void WaveJudge() {

		if ( waveDoneFlag == true || EnemyNumber < 1 ) {

			waveManager.SendMessage("WaveStart");

		}

	}




	void GameOverJudge() {

		if ( player1Flag == false && player2Flag == false && player3Flag == false && player4Flag == false ) {

			GameOver();

		}

		if ( BallNumber <= 0 ) {

			GameOver();

		}

	}




	void GameOver() {

		gameOverText.gameObject.SetActive(true);

		gameOverFlag = true;

		FindObjectOfType<ScoreController>().SaveScore();

        _soundKeeper.GetComponent<SoundKeeper>().PlayGameOverSound();
		waveManager.SetActive(false);
		GameObject[] Balls = GameObject.FindGameObjectsWithTag("Ball");  
		foreach(GameObject obj in Balls)
		{
			obj.SetActive(false);
		}

	}




	void Restart() {

		Application.LoadLevel("EndlessMode");

	}




	public void GameQuit() {

		Application.LoadLevel("Title");

	}

}
