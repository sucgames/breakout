using System;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
class SoundKeeper : MonoBehaviour
{
    public AudioClip enemyDamageSound;
    public AudioClip enemyShotSound;
    public AudioClip gameOverSound;

    public AudioClip spawnBallSound;
    public AudioClip bulletShotSound;

    public AudioClip waveStartSound;
    public AudioClip ballCollideSound;
    public AudioClip menuMoveDownSound;
    public AudioClip menuMoveUpSound;
    public AudioClip menuSubmitSound;

    private AudioSource _audio;

    void Awake()
    {
        _audio = GetComponent<AudioSource>();
    }

    public void PlayEnemyDestroySound()
    {
        _audio.PlayOneShot(enemyDamageSound);
    }

    public void PlayEnemyShotSound()
    {
        _audio.PlayOneShot(enemyShotSound);
    }

    public void PlayGameOverSound()
    {
        _audio.PlayOneShot(gameOverSound);
    }

    public void PlaySpawnBallSound()
    {
        _audio.PlayOneShot(spawnBallSound);
    }
    public void PlayBulletShotSound()
    {
        _audio.PlayOneShot(bulletShotSound);
    }
    public void PlayBallCollideSound()
    {
        _audio.PlayOneShot(ballCollideSound);
    }
    public void PlayWaveStartSound()
    {
        _audio.PlayOneShot(waveStartSound);
    }

    public void PlayMenuMoveDownSound()
    {
        _audio.PlayOneShot(menuMoveDownSound);
    }

    public void PlayMenuMoveUpSound()
    {
        _audio.PlayOneShot(menuMoveUpSound);
    }

    public void PlayMenuSubmitSound()
    {
        _audio.PlayOneShot(menuSubmitSound);
    }
}