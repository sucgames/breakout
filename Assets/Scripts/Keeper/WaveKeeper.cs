﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveKeeper : MonoBehaviour {

	public GameObject[] waveObject;

	public int waveNumber;

    ScoreView waveView;

    private void Awake()
    {
        waveView = GameObject.Find("WaveText").AddComponent<ScoreView>();
    }

    void Start() {
    }




	void Update() {
        
    }




	void WaveGenerate() {

		GameObject obj = (GameObject)Instantiate(waveObject[waveNumber], transform.position, transform.rotation);

		obj.transform.parent = transform;

        //下の画面のウェーブ表示
        waveView.UpdateScore(waveNumber + 1);


        if ( waveNumber < 7 ) {
			
			waveNumber = waveNumber + 1;

		}


	}

}
