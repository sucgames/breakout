﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BallSettingData", menuName = "BallSettingData")]
public class BallSettingData : ScriptableObject
{
    [Header("速度設定系")]
    [SerializeField, Tooltip("初速設定")] private float initial_velocity = 0;//初速設定
    [SerializeField, Tooltip("最大速度設定")] private float maxVelocity = 0;//最大速度
    [SerializeField, Tooltip("最低速度設定")] private float minVelocity = 0;//最低速度
    [SerializeField, Tooltip("加速最大速度設定")] private float add_maxVelocity = 0;//最大速度
    [SerializeField, Tooltip("加速最低速度設定")] private float add_minVelocity = 0;//最低速度
    [SerializeField, Tooltip("加速効果時間")] private float add_Speed_time = 0;//効果時間
    [Space(1)]
    [Header("角度制限系")]
    [SerializeField, Range(0, 90), Tooltip("水平上限角度0～90")] private float horizontal_limit_min = 0;
    [SerializeField, Range(90, 180), Tooltip("水平上限角度90～180")] private float horizontal_limit_max = 90;
    [SerializeField, Range(0, 90), Tooltip("垂直上限角度0～90")] private float vertical_limit_min = 0;
    [SerializeField, Range(90, 180), Tooltip("垂直上限角度90～180")] private float vertical_limit_max = 180;
    [SerializeField, Tooltip("反射角にランダムを適用するか")] private bool useRandam = false;
    [SerializeField, Range(0, 5), Tooltip("反射角")] private float randamvalue = 0;

    public float Initial_velocity { get => initial_velocity;}
    public float MaxVelocity { get => maxVelocity;}
    public float MinVelocity { get => minVelocity;}
    public float Add_maxVelocity { get => add_maxVelocity;}
    public float Add_minVelocity { get => add_minVelocity;}
    public float Add_Speed_time { get => add_Speed_time;}
    public float Horizontal_limit_min { get => horizontal_limit_min;}
    public float Horizontal_limit_max { get => horizontal_limit_max;}
    public float Vertical_limit_min { get => vertical_limit_min;}
    public float Vertical_limit_max { get => vertical_limit_max;}
    public bool UseRandam { get => useRandam;}
    public float Randamvalue { get => randamvalue;}
}
