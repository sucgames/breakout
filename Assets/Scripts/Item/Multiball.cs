﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Multiball : MonoBehaviour {

	Rigidbody rb;

	public GameObject multiballPrefab;
	public GameObject explosionPrefab;

	public int multiballNumber;

	public bool multiballFlag;


	void Start() {

		multiballFlag = true;

		rb = this.GetComponent<Rigidbody> ();

		Vector3 force = new Vector3 (Random.Range(-10.0f, 10.0f), 0.0f, -5.0f);

		rb.AddForce (force, ForceMode.Impulse);

	}




	void Update() {

	}




	void BallGenerate() {

		if ( multiballFlag == true ) {
			
			multiballFlag = false;

			StartCoroutine(Delay());

		}

	}




	void BallDelete() {

		Destroy (this.gameObject);

	}




	void OnCollisionEnter(Collision collision) {

		if ( collision.gameObject.tag == "Player1" || collision.gameObject.tag == "Player2"
			 || collision.gameObject.tag == "Player3" || collision.gameObject.tag == "Player4"
			 || collision.gameObject.tag == "Ball" ) {

			BallGenerate();

		}

	}




	IEnumerator Delay() {

		Instantiate(explosionPrefab, transform.position, transform.rotation);

		for(int i = 1; i <= multiballNumber; ++i) {

			Instantiate(multiballPrefab, transform.position, transform.rotation);

			yield return new WaitForSeconds(0.001f);

		}

		Destroy (this.gameObject);

	}

}
