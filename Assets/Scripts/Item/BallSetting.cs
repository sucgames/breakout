﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.SliderJoint.Scripts;

public class BallSetting : MonoBehaviour
{
    [Header("加速設定")]
    [SerializeField] private VelocitySys velocitySys = new VelocitySys();
    [Header("角度設定")]
    [SerializeField] private AngleSys angleSys = new AngleSys();
    [Header("当たり判定系")]
    [SerializeField] private HittingSys hittingSys = new HittingSys();
    [SerializeField] private GameObject ballcore;
    [SerializeField] private ParticleSystem bollEffect1;
    [SerializeField] private ParticleSystem bollEffect2;

    //メンバー変数
    //パブリック
    public GameObject gameKeeper;
    //プライベート
    private Rigidbody rb;
    private BallStateType ballState;
    private Timer timer;
    private bool effectFlag = false;

    //インスペクター表示用
    [SerializeField] private Vector3 vector3;
    [SerializeField] private float angla;
    [SerializeField] private float velocity;

    public enum BallStateType
    {
        NORMAL,
        PENETRAT
    }

    public VelocitySys Velocitysys { get => velocitySys; set => velocitySys = value; }

    void Start()
    {
        ballState = BallStateType.NORMAL;
        gameKeeper = GameObject.Find("GameKeeper");
        gameKeeper.SendMessage("Ball_Spawn");
        rb = gameObject.GetComponent<Rigidbody>();
        velocitySys.Rb = rb;
        angleSys.Rb = rb;
        velocitySys.InitialAcceleration();
        hittingSys.Ballcore = ballcore;
        timer = new Timer(Timer.TimerType.CountDown, velocitySys.Get_add_Speed_time);
        PenetratEffectStop();
    }
    private void FixedUpdate()
    {
        ChangeDefaultLayer(timer.Check(0));
        velocitySys.VelocitySysCore(ballState);
        angleSys.AngleControl();
        hittingSys.PenetrationPreference(ballState);
        InspecterView();
        PenetratEffectPlay();
        timer.update();
    }
    private void InspecterView()
    {
        vector3 = rb.velocity;
        angla = Vector3.Angle(Vector3.forward, rb.velocity);
        velocity = rb.velocity.magnitude;
    }
    private void OnCollisionEnter(Collision collision)
    {
        angleSys.Hitflag = true;
        hittingSys.SendEnemyDamage(collision);
        ballState = hittingSys.HitPlayer(collision, ballState, timer);

        gameKeeper.GetComponent<GameKeeper>().Ball_Collide();
    }
    private void OnTriggerEnter(Collider collision)
    {
        hittingSys.SendEnemyDamage(collision);
        ballState = hittingSys.HitPlayer(collision, ballState, timer);
    }
    private void BallDelete()
    {
        gameKeeper.SendMessage("Ball_Delete");
        Destroy(this.gameObject);
    }
    public void LoadBallData(BallSettingData data)
    {
        velocitySys = new VelocitySys(data);
        angleSys = new AngleSys(data);
        hittingSys = new HittingSys();
        velocitySys.Rb = rb;
        angleSys.Rb = rb;
        hittingSys.Ballcore = ballcore;
    }
    private void ChangeDefaultLayer(bool flag)
    {
        if (flag == true) {
            ballState = BallStateType.NORMAL;
            PenetratEffectStop();
            effectFlag = false;
        }
    }
    private void PenetratEffectPlay()
    {
        if (effectFlag == false && ballState == BallStateType.PENETRAT)
        {
            bollEffect1.Play(true);
            bollEffect2.Play(true);
            effectFlag = true;
        }
    }
    private void PenetratEffectStop()
    {
        bollEffect1.Stop(false);
        bollEffect2.Stop(false);
    }

    [System.Serializable]
    public class VelocitySys
    {
        [SerializeField] private float initial_velocity = 0;
        [SerializeField] private float maxVelocity = 0;//最大速度
        [SerializeField] private float minVelocity = 0;//最低速度
        [SerializeField] private float add_maxVelocity = 0;//最大速度
        [SerializeField] private float add_minVelocity = 0;//最低速度
        [SerializeField] private float add_Speed_time = 0;//効果時間
        private Rigidbody rb;
        private bool is_max_release = false;
        private float time = 0;
        private float maxlimit = 0;
        private float minlimit = 0;
        //アクセッサー
        public Rigidbody Rb { get => rb; set => rb = value; }
        public float Get_add_Speed_time { get => add_Speed_time; }
        //何もしないvoidコンストラクタ
        public VelocitySys() { }
        //データロード用コンストラクタ
        public VelocitySys(BallSettingData ballSettingData)
        {
            initial_velocity = ballSettingData.Initial_velocity;
            maxVelocity = ballSettingData.MaxVelocity;
            minVelocity = ballSettingData.MinVelocity;
            add_maxVelocity = ballSettingData.Add_maxVelocity;
            add_minVelocity = ballSettingData.Add_minVelocity;
            add_Speed_time = ballSettingData.Add_Speed_time;
        }
        //ボール初速設定
        public void InitialAcceleration()
        {
            Vector3 angle = new Vector3(Random.Range(-20.0f, 20.0f), 0.0f, 5.0f).normalized;
            Vector3 force = angle * initial_velocity;
            rb.AddForce(force, ForceMode.Impulse);
        }
        //外部呼出し用
        public void VelocitySysCore(BallStateType ballState)
        {
            float velocity = rb.velocity.magnitude;
            VelocitySetting(ballState);
            velocity = VelocityAdjuster(velocity);
            rb.velocity = velocity * rb.velocity.normalized;
        }
        //ボールの運動停止
        public void VelocityStop()
        {
            rb.velocity = Vector3.zero;
        }
        //ボールの追加加速用
        public void LimitedVelocityChange(bool flag)
        {
            if (flag == true)
            {
                is_max_release = true;
                time = add_Speed_time;
            }

        }
        private void VelocitySetting(BallStateType ballState)
        {
            switch (ballState)
            {
                case BallStateType.NORMAL:
                    maxlimit = maxVelocity;
                    minlimit = minVelocity;
                    break;
                case BallStateType.PENETRAT:
                    maxlimit = add_maxVelocity;
                    minlimit = add_minVelocity;
                    break;
            }
        }
        //加速維持用（外部参照負荷）
        private float VelocityAdjuster(float velocity)
        {
            float _velocty = velocity;
            //速度が一定異常になった場合
            if (velocity > maxlimit)
            {
                _velocty = maxlimit;
            }
            //加速度が一定以下になった場合
            if (velocity < minlimit)
            {
                _velocty = minlimit;
            }
            return _velocty;
        }
    }
    [System.Serializable]
    class AngleSys
    {
        [SerializeField] private float horizontal_limit_min = 0;
        [SerializeField] private float horizontal_limit_max = 0;
        [SerializeField] private float vertical_limit_min = 0;
        [SerializeField] private float vertical_limit_max = 0;
        [SerializeField] private bool useRandam = false;
        [SerializeField, Range(0, 5)] private float Randamvalue = 0;
        private Rigidbody rb;
        private bool hitflag = false;
        public Rigidbody Rb { get => rb; set => rb = value; }
        public bool Hitflag { set => hitflag = value; }
        //何もしないvoidコンストラクタ
        public AngleSys() { }
        //データロード用コンストラクタ
        public AngleSys(BallSettingData ballSettingData)
        {
            horizontal_limit_max = ballSettingData.Horizontal_limit_max;
            horizontal_limit_min = ballSettingData.Horizontal_limit_min;
            vertical_limit_min = ballSettingData.Vertical_limit_min;
            vertical_limit_max = ballSettingData.Vertical_limit_max;
            useRandam = ballSettingData.UseRandam;
            Randamvalue = ballSettingData.Randamvalue;
        }
        public void AngleControl()
        {
            float rad2 = Vector3.Angle(Vector3.right, rb.velocity) * Mathf.Deg2Rad;
            float sign = Mathf.Cos(rad2) > 0 ? 1 : -1;
            float angle = Vector3.Angle(Vector3.forward, rb.velocity);//角度取得取得
            if (angle >= horizontal_limit_min && angle <= horizontal_limit_max)//min～maxいないなら入る
            {
                float comparison_value = (horizontal_limit_min + horizontal_limit_max) / 2;
                if (angle > comparison_value)
                {
                    angle = horizontal_limit_max + 1;
                }
                else if (angle < comparison_value)
                {
                    angle = horizontal_limit_min - 1;
                }
            }
            if (angle < vertical_limit_min && angle >= 0)//1
            {
                angle = vertical_limit_min + 1;
            }
            if (angle > vertical_limit_max && angle <= 180)//179
            {
                angle = vertical_limit_max - 1;
            }
            angle = RandAngla(angle);
            float rad = angle * Mathf.Deg2Rad;
            Vector3 vec = new Vector3(Mathf.Sin(rad) * sign, 0, Mathf.Cos(rad));
            //速度設定
            rb.velocity = vec * rb.velocity.magnitude;
        }
        private float RandAngla(float angle)
        {
            float randAngla = angle;
            if (useRandam == true && hitflag == true)
            {
                randAngla += Random.Range(0, Randamvalue);
                Hitflag = false;
            }
            return randAngla;
        }
    }
    [System.Serializable]
    class HittingSys
    {
        private GameObject ballcore;
        public GameObject Ballcore { set => ballcore = value; }

        public HittingSys() { }
        public HittingSys(BallSettingData ballSettingData) { }
        public void PenetrationPreference(BallStateType ballState)
        {
            switch (ballState)
            {
                case BallStateType.NORMAL:
                    ballcore.layer = LayerMask.NameToLayer("Ball");
                    break;
                case BallStateType.PENETRAT:
                    ballcore.layer = LayerMask.NameToLayer("Pene_ball");
                    break;
            }
        }
        public BallStateType HitPlayer(Collision _object, BallStateType ballState, Timer timer)
        {
            return HitPlayerCore(_object.gameObject, ballState, timer);
        }
        public BallStateType HitPlayer(Collider _object, BallStateType ballState, Timer timer)
        {
            return HitPlayerCore(_object.gameObject, ballState, timer);
        }
        private BallStateType HitPlayerCore(GameObject _Object, BallStateType ballState, Timer timer)
        {
            if (_Object.layer == LayerMask.NameToLayer("Player") && _Object.GetComponentInParent<SliderJoint>().UseMotor && ballState == BallStateType.NORMAL)
            {
                timer.ReStert();
                return BallStateType.PENETRAT;
            } /*else if (_Object.layer == LayerMask.NameToLayer("Player") && _Object.GetComponentInParent<SliderJoint>().UseMotor && ballState == BallStateType.NORMAL)
            {
                timer.ReStert();
                return BallStateType.PENETRAT;
            }*/
            else
            {
                return ballState;
            }
        }
        public void SendEnemyDamage(Collision _object)
        {
            SendEnemyDamageCore(_object.gameObject);
        }
        public void SendEnemyDamage(Collider _object)
        {
            SendEnemyDamageCore(_object.gameObject);
        }
        private void SendEnemyDamageCore(GameObject _object)
        {
            if (_object.gameObject.tag == "Enemy")
            {
                _object.gameObject.SendMessage("EnemyDamage");
            }
        }
    }
    class Timer
    {
        public enum TimerType
        {
            CountDown,
            CountUP
        }

        private TimerType timerType;
        private float baseTime = 0;
        private float time = 0;

        public Timer(TimerType _timerType, float time_value)
        {
            baseTime = time_value;
            timerType = _timerType;
        }

        public float GetTime()
        {
            return time;
        }
        public float GetbaseTime()
        {
            return baseTime;
        }
        
        public void ReStert(float time_value)
        {
            time = time_value;
        }
        public void AddTime()
        {
            time += baseTime;
        }
        public void ReStert()
        {
            time = baseTime;
        }
        public void update()
        {
            switch (timerType)
            {
                case TimerType.CountDown:
                    time = time - Time.fixedDeltaTime;
                    break;
                case TimerType.CountUP:
                    time = time - Time.fixedDeltaTime;
                    break;
            }
        }
        public bool Check(float check_Time)
        {
            switch (timerType)
            {
                case TimerType.CountDown:
                    return check_Time > time;
                case TimerType.CountUP:
                    return check_Time < time;
                default:
                    return false;
            }
        }
    }
}