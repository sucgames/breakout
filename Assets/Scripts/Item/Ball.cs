﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

	Rigidbody rb;

	public GameObject gameKeeper;


	void Start() {

		gameKeeper = GameObject.Find("GameKeeper");
		gameKeeper.SendMessage("Ball_Spawn");

		rb = this.GetComponent<Rigidbody> ();

		Vector3 force = new Vector3 (Random.Range(-20.0f, 20.0f), 0.0f, 5.0f);

		rb.AddForce (force, ForceMode.Impulse);

	}




	void Update() {

	}




	void BallDelete() {

		gameKeeper.SendMessage("Ball_Delete");

		Destroy (this.gameObject);

	}




	void OnCollisionEnter(Collision collision) {

		if ( collision.gameObject.tag == "Enemy" ) {

			collision.gameObject.SendMessage("EnemyDamage");

		}

	}

}
