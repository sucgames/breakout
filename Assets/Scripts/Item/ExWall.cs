﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExWall : MonoBehaviour {

	public GameObject waveManager;
	public GameObject exWall;

	public float timer;
	public float timerCache;

	public int endurance;

	public bool damageFlag;


    void Start() {

		timerCache = timer;

		damageFlag = false;
        
    }




    void Update() {

		timerCache = timerCache - Time.deltaTime;

		if ( timerCache < 0.0f && endurance == 0 ) {

			if ( damageFlag == true ) {

				ExWallEnabled();

			} else if ( damageFlag == false ) {

				ExWallDisabled();

			}

		}
        
    }
	
	
	
	
	void ExWallEnabled() {

		exWall.gameObject.SetActive(true);

		damageFlag = false;

		timerCache = timer;

	}
	
	
	
	
	void ExWallDisabled() {

		exWall.gameObject.SetActive(false);

		damageFlag = true;

		timerCache = timer;

	}




	void Endurance1() {

		endurance = 0;

		TimeReset();

	}




	void Endurance3() {

		endurance = 2;

		TimeReset();

	}




	void Endurance5() {

		endurance = 4;

		TimeReset();

	}




	void EnduranceEternal() {

		endurance = 1000000;

		TimeReset();

	}




	void TimeReset() {

		exWall.gameObject.SetActive(true);

		timerCache = timer;

	}




	void BallHit() {

		endurance = endurance - 1;

		if ( endurance < 0 ) {

			waveManager.SendMessage("ExWall_Delete");

			//Wave100以降用のテスト
			Destroy (this.gameObject);
			//テストが終わったら消します

		}

	}




	void OnCollisionEnter(Collision collision) {

		if ( collision.gameObject.tag == "Ball" ) {

			BallHit();

		}

	}
		
}
