﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion_Collision_L : MonoBehaviour {


    void Start() {

		StartCoroutine(Delay());
        
    }




    void Update() {
        
    }




	void OnCollisionEnter(Collision collision) {

		if ( collision.gameObject.tag == "Enemy" ) {

			collision.gameObject.SendMessage("EnemyDamage");

		}

		if ( collision.gameObject.tag == "Block" ) {

			collision.gameObject.SendMessage("BlockDestroy");

		}

	}




	IEnumerator Delay() {

		yield return new WaitForSeconds( Random.Range(0.01f, 0.2f) );

		GetComponent<SphereCollider>().enabled = true;

	}

}