﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionController : MonoBehaviour {

	public GameObject iconGamePlayer;
	public GameObject iconBgmVolume;
	public GameObject iconSeVolume;
	public GameObject iconEffect;
	public GameObject iconBack;

	public GameObject gamePlayerMenu;

	public float horizontalKeyInput;
	public float horizontalKeyInput2;
	public float verticalKeyInput;
	public float verticalKeyInput2;

	public int horizontalJudge;
	public int verticalJudge;

	public int gamePlayerNumber;
	public int bgmVolumeNumber;
	public int seVolumeNumber;
	public int effectNumber;

	public bool horizontalAnalogFlag;
	public bool horizontalDigitalFlag;
	public bool verticalAnalogFlag;
	public bool verticalDigitalFlag;

	private string gamePlayerMenuKey = "gamePlayerMenu";


    void Start() {

		horizontalAnalogFlag = true;

		horizontalDigitalFlag = true;

		verticalAnalogFlag = true;

		verticalDigitalFlag = true;

		gamePlayerNumber = PlayerPrefs.GetInt (gamePlayerMenuKey, 1);

		FirstCheck();
        
    }




    void Update() {

		if ( Input.GetButtonDown ("Submit") ) {

			GameStart();

		}




		if ( Input.GetButtonDown ("Back") ) {

			verticalJudge = 5;

			GameStart();

		}




		if ( Input.GetButtonDown ("Cancel") ) {

			GameQuit();

		}








		if ( Input.GetKeyDown (KeyCode.LeftArrow) ) {

			MoveLeft();

		}

		if ( Input.GetKeyDown (KeyCode.RightArrow) ) {

			MoveRight();

		}




		horizontalKeyInput = Input.GetAxis("HorizontalAnalogPad");
		if ( horizontalKeyInput < -0.5f ) {

			if ( horizontalAnalogFlag == true ) {

				horizontalAnalogFlag = false;

				MoveLeft();

			}

		} else if ( horizontalKeyInput > 0.5f ) {

			if ( horizontalAnalogFlag == true ) {

				horizontalAnalogFlag = false;

				MoveRight();

			}

		} else if ( verticalKeyInput2 > -0.5f && verticalKeyInput2 < 0.5f ) {

			horizontalAnalogFlag = true;

		}




		horizontalKeyInput2 = Input.GetAxis("HorizontalDPad");
		if ( horizontalKeyInput2 < -0.5f ) {

			if ( horizontalDigitalFlag == true ) {

				horizontalDigitalFlag = false;

				MoveLeft();

			}

		} else if ( horizontalKeyInput2 > 0.5f ) {

			if ( horizontalDigitalFlag == true ) {

				horizontalDigitalFlag = false;

				MoveRight();

			}

		} else if ( verticalKeyInput2 > -0.5f && verticalKeyInput2 < 0.5f ) {

			horizontalDigitalFlag = true;

		}








		if ( Input.GetKeyDown (KeyCode.UpArrow) ) {

			MoveUp();

		}

		if ( Input.GetKeyDown (KeyCode.DownArrow) ) {

			MoveDown();

		}




		verticalKeyInput = Input.GetAxis("VerticalAnalogPad");
		if ( verticalKeyInput < -0.5f ) {

			if ( verticalAnalogFlag == true ) {

				verticalAnalogFlag = false;

				MoveUp();

			}

		} else if ( verticalKeyInput > 0.5f ) {

			if ( verticalAnalogFlag == true ) {

				verticalAnalogFlag = false;

				MoveDown();

			}

		} else if ( verticalKeyInput > -0.5f && verticalKeyInput < 0.5f ) {

			verticalAnalogFlag = true;

		}


		verticalKeyInput2 = Input.GetAxis("VerticalDPad");
		if ( verticalKeyInput2 < -0.5f ) {

			if ( verticalDigitalFlag == true ) {

				verticalDigitalFlag = false;

				MoveUp();

			}

		} else if ( verticalKeyInput2 > 0.5f ) {

			if ( verticalDigitalFlag == true ) {

				verticalDigitalFlag = false;

				MoveDown();

			}

		} else if ( verticalKeyInput2 > -0.5f && verticalKeyInput2 < 0.5f ) {

			verticalDigitalFlag = true;

		}
				        
    }




	void MoveLeft() {

		horizontalJudge = 1;

		SettingCheck();

	}




	void MoveRight() {

		horizontalJudge = 2;

		SettingCheck();

	}




	void MoveUp() {

		verticalJudge = verticalJudge - 1;

		if ( verticalJudge < 1 ) {

			verticalJudge = 5;

		}

		MoveCheck();

	}




	void MoveDown() {

		verticalJudge = verticalJudge + 1;

		if ( verticalJudge > 5 ) {

			verticalJudge = 1;

		}

		MoveCheck();

	}




	void SettingCheck() {

		if ( verticalJudge == 1 && horizontalJudge == 1 ) {

			if ( gamePlayerNumber > 1 ) {
				
				gamePlayerNumber = gamePlayerNumber - 1;

				GamePlayerNumberCheck();

			}

		} else if ( verticalJudge == 1 && horizontalJudge == 2 ) {

			if ( gamePlayerNumber < 4 ) {
				
				gamePlayerNumber = gamePlayerNumber + 1;

				GamePlayerNumberCheck();

			}
					
		}


		if ( verticalJudge == 2 && horizontalJudge == 1 ) {

			if ( bgmVolumeNumber > 0 ) {

				bgmVolumeNumber = bgmVolumeNumber - 1;

			}

		} else if ( verticalJudge == 2 && horizontalJudge == 2 ) {

			if ( bgmVolumeNumber < 100 ) {

				bgmVolumeNumber = bgmVolumeNumber + 1;

			}

		}


		if ( verticalJudge == 3 && horizontalJudge == 1 ) {

			if ( seVolumeNumber > 0 ) {

				seVolumeNumber = seVolumeNumber - 1;

			}

		} else if ( verticalJudge == 3 && horizontalJudge == 2 ) {

			if ( seVolumeNumber < 100 ) {

				seVolumeNumber = seVolumeNumber + 1;

			}

		}


		if ( verticalJudge == 4 && horizontalJudge == 1 ) {

			effectNumber = 0;

		} else if ( verticalJudge == 4 && horizontalJudge == 2 ) {

			effectNumber = 1;

		}

	}




	void MoveCheck() {

		if ( verticalJudge == 1 ) {

			iconGamePlayer.gameObject.SetActive(true);
			iconBgmVolume.gameObject.SetActive(false);
			iconSeVolume.gameObject.SetActive(false);
			iconEffect.gameObject.SetActive(false);
			iconBack.gameObject.SetActive(false);

		} else if ( verticalJudge == 2 ) {

			iconGamePlayer.gameObject.SetActive(false);
			iconBgmVolume.gameObject.SetActive(true);
			iconSeVolume.gameObject.SetActive(false);
			iconEffect.gameObject.SetActive(false);
			iconBack.gameObject.SetActive(false);

		} else if ( verticalJudge == 3 ) {

			iconGamePlayer.gameObject.SetActive(false);
			iconBgmVolume.gameObject.SetActive(false);
			iconSeVolume.gameObject.SetActive(true);
			iconEffect.gameObject.SetActive(false);
			iconBack.gameObject.SetActive(false);

		} else if ( verticalJudge == 4 ) {

			iconGamePlayer.gameObject.SetActive(false);
			iconBgmVolume.gameObject.SetActive(false);
			iconSeVolume.gameObject.SetActive(false);
			iconEffect.gameObject.SetActive(true);
			iconBack.gameObject.SetActive(false);

		} else if ( verticalJudge == 5 ) {

			iconGamePlayer.gameObject.SetActive(false);
			iconBgmVolume.gameObject.SetActive(false);
			iconSeVolume.gameObject.SetActive(false);
			iconEffect.gameObject.SetActive(false);
			iconBack.gameObject.SetActive(true);

		}

	}




	void FirstCheck() {

		GamePlayerNumberCheck();

	}




	void GamePlayerNumberCheck() {

		if ( gamePlayerNumber == 1 ) {

			gamePlayerMenu.SendMessage("Menu1P");

			PlayerPrefs.SetInt (gamePlayerMenuKey, gamePlayerNumber);

		} else if ( gamePlayerNumber == 2 ) {

			gamePlayerMenu.SendMessage("Menu2P");

			PlayerPrefs.SetInt (gamePlayerMenuKey, gamePlayerNumber);

		} else if ( gamePlayerNumber == 3 ) {

			gamePlayerMenu.SendMessage("Menu3P");

			PlayerPrefs.SetInt (gamePlayerMenuKey, gamePlayerNumber);

		} else if ( gamePlayerNumber == 4 ) {

			gamePlayerMenu.SendMessage("Menu4P");

			PlayerPrefs.SetInt (gamePlayerMenuKey, gamePlayerNumber);

		}

	}




	void GameStart() {

		if ( verticalJudge == 5 ) {
			
			Application.LoadLevel("Title");

		}

	}




	void GameQuit() {

		Application.Quit();

	}

}