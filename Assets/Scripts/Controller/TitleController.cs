﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleController : MonoBehaviour {

	public GameObject titleText;
	public GameObject pushStartButtonText;
	public GameObject menuText;

	public float waitTime;
	public float scoreRankingWaitTime;

	public bool gameStartFlag;


    void Start() {

		gameStartFlag = false;

		StartCoroutine(Delay());
        
    }




    void Update() {

		if ( Input.GetButtonDown ("Submit") && gameStartFlag == false ) {

			MenuStart();

		}




		if ( Input.GetButtonDown ("Cancel") ) {

			GameQuit();

		}


		if ( Input.GetKeyDown ("r") ) {

			Restart();

		}


		if ( Input.GetKeyDown ("q") ) {

			ScoreRankingLoad();

		}
				        
    }




	void MenuStart() {

		titleText.gameObject.SetActive(false);

		pushStartButtonText.gameObject.SetActive(false);

		menuText.gameObject.SetActive(true);

		gameStartFlag = true;

	}




	public void GameStart() {

		Application.LoadLevel("EndlessMode");

	}




	void Restart() {

		Application.LoadLevel("Title");

	}




	void GameQuit() {

		Application.Quit();

	}




	void ScoreRankingLoad() {

		Application.LoadLevel("ScoreRanking");

	}




	IEnumerator Delay() {

		yield return new WaitForSeconds(waitTime);

		if ( gameStartFlag == false ) {

			pushStartButtonText.gameObject.SetActive(true);

		}

		yield return new WaitForSeconds(scoreRankingWaitTime);

		ScoreRankingLoad();

	}

}