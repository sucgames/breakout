﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CIController : MonoBehaviour {

	public GameObject ciObject;

	public float waitTime;


    void Start() {

		StartCoroutine(Delay());
        
    }




    void Update() {

		if ( Input.GetButtonDown ("Submit") ) {

			GameStart();

		}




		if ( Input.GetButtonDown ("Back") ) {

			GameStart();

		}




		if ( Input.GetButtonDown ("Cancel") ) {

			GameQuit();

		}
				        
    }




	public void GameStart() {

		Application.LoadLevel("Title");

	}




	void GameQuit() {

		Application.Quit();

	}




	IEnumerator Delay() {

		yield return new WaitForSeconds(1.0f);

		ciObject.gameObject.SetActive(true);

		yield return new WaitForSeconds(waitTime);

		GameStart();

	}

}