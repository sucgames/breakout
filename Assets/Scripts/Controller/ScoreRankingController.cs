﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreRankingController : MonoBehaviour {

	public float waitTime;

	public int demoFlag;


    void Start() {

		StartCoroutine(Delay());
        
    }




    void Update() {

		if ( Input.GetButtonDown ("Submit") ) {

			GameStart();

		}




		if ( Input.GetButtonDown ("Cancel") ) {

			GameQuit();

		}
				        
    }




	public void GameStart() {

		Application.LoadLevel("Title");

	}




	void Demo01Load() {

		Application.LoadLevel("Demo01");

	}




	void GameQuit() {

		Application.Quit();

	}




	IEnumerator Delay() {

		yield return new WaitForSeconds(waitTime);

		demoFlag = Random.Range(1, 2);

		if ( demoFlag == 1 ) {

			Demo01Load();

		}

	}

}