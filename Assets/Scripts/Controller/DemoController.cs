﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoController : MonoBehaviour {

	public float waitTime;

	public int demoNumber;


    void Start() {

		StartCoroutine(Delay());
        
    }




    void Update() {

		if ( Input.GetButtonDown ("Submit") ) {

			GameStart();

		}




		if ( Input.GetButtonDown ("Cancel") ) {

			GameQuit();

		}
				        
    }




	public void GameStart() {

		Application.LoadLevel("CI");

	}




	void NextDemo() {

		if ( demoNumber == 1 ) {

			Application.LoadLevel("Demo02");

		} else if ( demoNumber == 2 ) {

			Application.LoadLevel("Demo03");

		} else if ( demoNumber == 3 ) {

			Application.LoadLevel("Demo04");

		} else if ( demoNumber == 4 ) {

			Application.LoadLevel("Demo05");

		} else if ( demoNumber == 5 ) {

			Application.LoadLevel("Demo06");

		} else if ( demoNumber == 6 ) {

			Application.LoadLevel("CI");

		}

	}




	void GameQuit() {

		Application.Quit();

	}




	IEnumerator Delay() {

		yield return new WaitForSeconds(waitTime);

		NextDemo();

	}

}