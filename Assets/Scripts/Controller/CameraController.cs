﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public float posX;
	public float posZ;

	public float timeScale;

	public float cameraShakeX;
	public float cameraShakeZ;
	public float cameraShakeXCache;
	public float cameraShakeZCache;


	public int cameraShakeNumber;


    void Start() {
        
    }




    void Update() {
        
    }




	void CameraShake() {

		transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);

		StartCoroutine(Delay());

	}




	IEnumerator Delay() {

		cameraShakeXCache = cameraShakeX;
		cameraShakeZCache = cameraShakeZ;

		Time.timeScale = timeScale;

		for(int i = 1; i < cameraShakeNumber; ++i) {

			posX = Random.Range(-cameraShakeXCache, cameraShakeXCache);
			posZ = Random.Range(-cameraShakeZCache, cameraShakeZCache);
			transform.localPosition = new Vector3(posX, 0.0f, posZ);

			cameraShakeXCache = cameraShakeXCache * 0.9f;
			cameraShakeZCache = cameraShakeZCache * 0.9f;

			yield return new WaitForSeconds(0.001f);

		}

		Time.timeScale = 1.0f;

		transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);

	}

}
