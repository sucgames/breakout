﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditController : MonoBehaviour {

	public float waitTime;


    void Start() {

		StartCoroutine(Delay());
        
    }




    void Update() {

		if ( Input.GetButtonDown ("Submit") ) {

			GameStart();

		}




		if ( Input.GetButtonDown ("Back") ) {

			GameStart();

		}




		if ( Input.GetButtonDown ("Cancel") ) {

			GameQuit();

		}
				        
    }




	public void GameStart() {

		Application.LoadLevel("Title");

	}




	void GameQuit() {

		Application.Quit();

	}




	IEnumerator Delay() {

		yield return new WaitForSeconds(waitTime);

		GameStart();

	}

}