using UnityEngine;
public class ScoreController : MonoBehaviour{
    [SerializeField]
    ScoreTextView View;

    private int _score = 0;
    [SerializeField]
    private ScoreKeeper ScoreKeeper;

    public void Start()
    {
        View = FindObjectOfType<ScoreTextView>();
        ScoreKeeper = FindObjectOfType<ScoreKeeper>();
        View.UpdateScore(_score);
    }

    public void OnScored(int score)
    {
        _score += score;
        View.UpdateScore(_score);
    }
    public void SaveScore()
    {
        int Hihgtcore = PlayerPrefs.GetInt(PlayerPrefsKey.HighScoreKey, 0);
        if(Hihgtcore < _score)
        {
            PlayerPrefs.SetInt(PlayerPrefsKey.HighScoreKey, _score);
        }
    }
}

