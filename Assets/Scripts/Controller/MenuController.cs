﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour {

	public GameObject iconEndlessMode;
	public GameObject iconChallengeMode;
	public GameObject iconOption;
	public GameObject iconCredit;
	public GameObject iconExit;

	public float verticalKeyInput;
	public float verticalKeyInput2;

	public int verticalJudge;

	public bool moveAnalogFlag;
	public bool moveDigitalFlag;

    private GameObject _soundKeeper;

    void Awake()
    {
        _soundKeeper = GameObject.Find("SoundKeeper");
    }

    void Start() {

		moveAnalogFlag = true;

		moveDigitalFlag = true;
        
    }




    void Update() {

		if ( Input.GetButtonDown ("Submit") ) {

			GameStart();

		}




		if ( Input.GetButtonDown ("Cancel") ) {

			GameQuit();

		}




		if ( Input.GetKeyDown (KeyCode.UpArrow) ) {

			MoveUp();

		}

		if ( Input.GetKeyDown (KeyCode.DownArrow) ) {

			MoveDown();

		}




		verticalKeyInput = Input.GetAxis("VerticalAnalogPad");
		if ( verticalKeyInput < -0.5f ) {

			if ( moveAnalogFlag == true ) {

				moveAnalogFlag = false;

				MoveUp();

			}

		} else if ( verticalKeyInput > 0.5f ) {

			if ( moveAnalogFlag == true ) {

				moveAnalogFlag = false;

				MoveDown();

			}

		} else if ( verticalKeyInput > -0.5f && verticalKeyInput < 0.5f ) {

			moveAnalogFlag = true;

		}


		verticalKeyInput2 = Input.GetAxis("VerticalDPad");
		if ( verticalKeyInput2 < -0.5f ) {
			
			if ( moveDigitalFlag == true ) {

				moveDigitalFlag = false;

				MoveUp();

			}

		} else if ( verticalKeyInput2 > 0.5f ) {

			if ( moveDigitalFlag == true ) {

				moveDigitalFlag = false;

				MoveDown();

			}

		} else if ( verticalKeyInput2 > -0.5f && verticalKeyInput2 < 0.5f ) {

			moveDigitalFlag = true;

		}
				        
    }




	void MoveUp() {

		verticalJudge = verticalJudge - 1;

		if ( verticalJudge < 1 ) {

			verticalJudge = 5;

		}

		MoveCheck();

        _soundKeeper.GetComponent<SoundKeeper>().PlayMenuMoveUpSound();
	}




	void MoveDown() {

		verticalJudge = verticalJudge + 1;

		if ( verticalJudge > 5 ) {

			verticalJudge = 1;

		}

		MoveCheck();

        _soundKeeper.GetComponent<SoundKeeper>().PlayMenuMoveDownSound();
	}




	void MoveCheck() {

		if ( verticalJudge == 1 ) {

			iconEndlessMode.gameObject.SetActive(true);
			iconChallengeMode.gameObject.SetActive(false);
			iconOption.gameObject.SetActive(false);
			iconCredit.gameObject.SetActive(false);
			iconExit.gameObject.SetActive(false);

		} else if ( verticalJudge == 2 ) {

			iconEndlessMode.gameObject.SetActive(false);
			iconChallengeMode.gameObject.SetActive(true);
			iconOption.gameObject.SetActive(false);
			iconCredit.gameObject.SetActive(false);
			iconExit.gameObject.SetActive(false);

		} else if ( verticalJudge == 3 ) {

			iconEndlessMode.gameObject.SetActive(false);
			iconChallengeMode.gameObject.SetActive(false);
			iconOption.gameObject.SetActive(true);
			iconCredit.gameObject.SetActive(false);
			iconExit.gameObject.SetActive(false);

		} else if ( verticalJudge == 4 ) {

			iconEndlessMode.gameObject.SetActive(false);
			iconChallengeMode.gameObject.SetActive(false);
			iconOption.gameObject.SetActive(false);
			iconCredit.gameObject.SetActive(true);
			iconExit.gameObject.SetActive(false);

		} else if ( verticalJudge == 5 ) {

			iconEndlessMode.gameObject.SetActive(false);
			iconChallengeMode.gameObject.SetActive(false);
			iconOption.gameObject.SetActive(false);
			iconCredit.gameObject.SetActive(false);
			iconExit.gameObject.SetActive(true);

		}

	}




	void GameStart() {

		if ( verticalJudge == 1 ) {

			Application.LoadLevel("EndlessMode");

		}

		if ( verticalJudge == 3 ) {
			
			Application.LoadLevel("Option");

		}

		if ( verticalJudge == 4 ) {

			Application.LoadLevel("Credit");

		}

		if ( verticalJudge == 5 ) {

			GameQuit();

		}

        _soundKeeper.GetComponent<SoundKeeper>().PlayMenuSubmitSound();
	}




	void GameQuit() {

		Application.Quit();

	}

}