using UnityEngine;
public class EnemyScoreController : MonoBehaviour{
    public int Combo = 1;
    public int Score = 50;

    public void Start()
    {
        this.gameObject.AddComponent<EnemyComboView>().Show(Combo);

        this.gameObject.AddComponent<EnemyScoreView>().Show(Score);
    }

    public void Setup(int combo, int score)
    {
        Combo = combo;
        Score = score;
    }
}

