using System.Collections.Generic;
using UnityEngine;
class ComboCurve : MonoBehaviour
{
    public GameObject EnemyScore;

    public List<ComboCurveConfig> repository = new List<ComboCurveConfig>();

    public ComboCurve(GameObject enemyScore)
    {
        EnemyScore = enemyScore;
    }

    // コンボ数に応じて倍率を計算する
    public float CalculateRate(int combo)
    {
        return 1.0f; // FIXME: dummy
    }

    public ComboCurveConfig Find()
    {
        foreach (ComboCurveConfig comboCurve in this.repository) {
            if (comboCurve.EnemyScoreName == EnemyScore.GetComponent<EnemyScore>().Name()) {
                return comboCurve;
            }
        }
        return null;
    }
}
