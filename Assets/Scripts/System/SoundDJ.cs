using UnityEngine;

class SoundDJ: MonoBehaviour {
    void OnEnemyDeath(GameObject enemy)
    {
        // enemyに応じた死亡SEを鳴らす
    }

    void OnEnemySpawn(GameObject enemy)
    {
        // enemyに応じた出撃SEを鳴らす
    }

    void OnWave(GameObject wave)
    {
        // 現在の音量をフェードアウトする
        // Waveのジングルを鳴らす(警報音)
        // WaveのBGMを鳴らす
    }
}