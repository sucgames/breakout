﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCounter : MonoBehaviour {
 
	public GameObject waveClearText;

	public float waveTime;


    void Start() {

		StartCoroutine(Delay());
        
    }




    void Update() {
        
    }




	IEnumerator Delay() {

		yield return new WaitForSeconds(waveTime);

		Instantiate(waveClearText, transform.position, transform.rotation);

	}

}
