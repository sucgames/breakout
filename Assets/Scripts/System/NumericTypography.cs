using System.Collections.Generic;
using UnityEngine;

public class NumericTypography {
    private GameObject[] _numerics;
    private Vector3 _origin;
    private float _kerning;

    public NumericTypography (GameObject[] numericObjects) {
        this._numerics = numericObjects;
    }

    public List<GameObject> MapToGameObject(int num) {
        return ReferNumericPrefab (SplitString (num.ToString ()));
    }

    // 数字に相当するプレハブを参照する
    private List<GameObject> ReferNumericPrefab (string[] numerics) {
        var numericSlot = new List<GameObject>();
        for (int i = 0; i < numerics.Length; i++) {
            numericSlot.Add(_numerics[int.Parse (numerics[i])]);
        }
        return numericSlot;
    }

    // 文字列を1文字ずつ分割した配列にする
    private string[] SplitString (string s) {
        string[] characters = new string[s.Length];

        for (int i = 0; i < s.Length; i++) {
            characters[i] = System.Convert.ToString (s[i]);
        }
        return characters;
    }
}