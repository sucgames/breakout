using UnityEngine;
class EnemyScore: MonoBehaviour
{
    [SerializeField]
    private int _score;
    public int Combo;
    [SerializeField, Tooltip("Enemy_A~Enemy_E")]
    public MonoBehaviour component; 

    public void Awake()
    {
        this.gameObject
            .AddComponent<EnemyScoreController>()
            .Setup(Score().ToInt(), Combo);
    }

    public Score Score() {
        return new Score(_score);
    }

    public string Name()
    {
        return component.GetType().Name;
    }
}