using System.Collections.Generic;
using UnityEngine;

class Score: ScriptableObject
{
    private int _score;

    public Score(int score)
    {
        _score = score;
    }

    public Score Multiply(float rate)
    {
        return new Score((int) (this._score * rate));
    }

    public Score Sum(int score)
    {
        return new Score(this._score + score);
    }
    public int ToInt()
    {
        return _score;
    }
}

