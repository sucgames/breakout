using System.Collections.Generic;
using UnityEngine;

public enum State {
    出撃,
    出撃中,
    攻撃開始,
    攻撃中,
    回避,
    回避中,

    撤退中,
    撤退,
    墜落,
}

// EnemyはBrainを持つ
// EnemyはController経由でBrainの決定した動きをする
// BrainはStateが変わると,Stateに紐づけられたAction.Actを行う
public class Brain : MonoBehaviour
{
    class Transition{
        readonly State current;
        readonly Action Action;

        public Transition (State state, Action action)
        {
            current = state;
            Action = action;
        }
    }

    [Header("トランジション")]
    [SerializeField, Tooltip("優先度の高い順で登録")]
    private Dictionary<Transition, State> transitions;

    private State current {get; }

    public void Process()
    {
        //current = State.出撃;
    }

    public State Next(Action Action)
    {
        Transition transition = new Transition(current, Action);
        State nextState;
        transitions.TryGetValue(transition, out nextState);
        return nextState;
    }

    public class Action {
        public Action()
        {

        }
    }
}

class StateController : MonoBehaviour{
    public State currentState;
    void Awake()
    {
        //enemy_A = GetComponent<Enemy> ();
    }

    public void SetupAI(bool aiActivation, List<Transform> waypoints)
    {
    }

    void Update() {
        //currentState.UpdateState(this);
    }

    void OnDrawGizmos() {
        //Gizmos.color = State.sceneGizmoColor;
    }
}

[CreateAssetMenu(menuName = "PluggableAI/State")]
class StateSetting : ScriptableObject {
    
    public Action[] actions;
    public Color sceneGizmoColor;

    public void UpdateState(StateController controller)
    {
        for (int i = 0; i< actions.Length; i++) {
            actions[i].Act(controller);
        }
    }
}
class Action: ScriptableObject{

    public void Act(StateController controller)
    {
    }
}
