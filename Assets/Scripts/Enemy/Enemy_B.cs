﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_B : MonoBehaviour {

	public GameObject gameKeeper;
	public GameObject explosionPrefab;
	public GameObject explosionPrefab2;
	public GameObject bulletPrefab;
	public GameObject scoreTextPrefab;
    public GameObject comboTextPrefab;

    public float moveDownSpeed;
	public float moveSideSpeed;
	public float waitTime;
	public float waitTimeCache;

	public float firstMove;

	public int moveType;

	public bool destroyFlag;
	public bool withdrawalFlag;

    //倒した時の素点
    [Header("倒した時の素点")]
    [SerializeField]
    private int rawSocre = 150;


    void Start() {

	    explosionPrefab.isStatic = true;
	    explosionPrefab2.isStatic = true;
	    bulletPrefab.isStatic = true;
	    scoreTextPrefab.isStatic = true;
        
		withdrawalFlag = false;

		destroyFlag = true;

		firstMove = Random.Range(15.0f, 25.0f);

		gameKeeper = GameObject.Find("GameKeeper");
		gameKeeper.SendMessage("EnemyB_Spawn");

		StartCoroutine(Delay());
        
    }




    void Update() {

		if ( withdrawalFlag == false ) {

			if ( transform.position.z > firstMove ) {

				transform.position -= transform.forward * moveDownSpeed * Time.deltaTime * 2;

				moveType = 0;

			}


			if ( moveType == 1 ) {

			} else if ( moveType == 2 ) {

				transform.position -= transform.forward * moveDownSpeed * Time.deltaTime;

			} else if ( moveType == 3 && transform.position.x >= -29.0f ) {

				transform.position -= transform.right * moveSideSpeed * Time.deltaTime;

			} else if ( moveType == 4 && transform.position.x <= 29.0f ) {

				transform.position += transform.right * moveSideSpeed * Time.deltaTime;

			}

		} else if ( withdrawalFlag == true ) {

			transform.rotation = Quaternion.Euler (0.0f, 180.0f, 0.0f);

			transform.position -= transform.forward * moveDownSpeed * 10.0f * Time.deltaTime;

		}
			
    }




	void EnemyDamage() {

		if ( destroyFlag == true ) {

			destroyFlag = false;

			Instantiate(explosionPrefab, transform.position, transform.rotation);
			Instantiate(explosionPrefab2, transform.position, transform.rotation);

			gameKeeper.SendMessage("EnemyB_Destroy");

            //スコアキーパーに死亡通知
            FindObjectOfType<ScoreKeeper>().SendEnemyDeath(gameObject, EnemyListEnum.Enemy_B, rawSocre);

            Destroy (this.gameObject);

		}

	}


    public void ScoreDisply(int score)
    {
		//表示処理
        var scoreTextobj = Instantiate(scoreTextPrefab, transform.position, transform.rotation);
        scoreTextobj.GetComponent<EnemyScoreView>().Show(score);

		//スコア加算
		FindObjectOfType<ScoreController>().OnScored(score);
    }

    public void ComboDisply(int combo)
    {
        var scoreTextobj = Instantiate(comboTextPrefab, transform.position + new Vector3(0, 0, -0.7f), transform.rotation);
        scoreTextobj.GetComponent<EnemyScoreView>().Show(combo);
    }


    void EnemyDelete() {

		gameKeeper.SendMessage("EnemyB_Delete");

		Destroy (this.gameObject);

	}




	void EnemyWithdrawal() {

		withdrawalFlag = true;

	}




	IEnumerator Delay() {

		while ( moveType >= 0 ) {

			moveType = Random.Range(1, 5);

			if ( moveType == 1 ) {

				Instantiate(bulletPrefab, transform.position, transform.rotation);
                BroadcastMessage("PlayBulletSound");

				waitTimeCache = waitTime * 2;

			} else if ( moveType == 2 ) {

				waitTimeCache = waitTime / 2;

			} else if ( moveType == 3 ) {

				waitTimeCache = waitTime * 3;

			} else if ( moveType == 4 ) {

				waitTimeCache = waitTime * 3;

			}

			yield return new WaitForSeconds(waitTimeCache);

		}

	}

}
