﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_D : MonoBehaviour {

	public GameObject gameKeeper;
	public GameObject explosionPrefab;
	public GameObject explosionPrefab2;
	public GameObject blockPrefab;
	public GameObject scoreTextPrefab;
    public GameObject comboTextPrefab;

    public float moveSpeed;
	public float rotationWaitMin;
	public float rotationWaitMax;

	public int moveType;

	public bool rotationFlag;
	public bool collisionFlag;
	public bool enemyDeleteFlag;
	public bool destroyFlag;
	public bool withdrawalFlag;

    //倒した時の素点
    [Header("倒した時の素点")]
    [SerializeField]
    private int rawSocre = 1000;


    void Start() {

	    explosionPrefab.isStatic = true;
	    explosionPrefab2.isStatic = true;
        blockPrefab.isStatic = true;
	    scoreTextPrefab.isStatic = true;
		withdrawalFlag = false;

		destroyFlag = true;

		gameKeeper = GameObject.Find("GameKeeper");
		gameKeeper.SendMessage("EnemyD_Spawn");

		rotationFlag = false;

		collisionFlag = false;

		enemyDeleteFlag = false;

		moveType = Random.Range(1, 3);

		if ( moveType == 1 ) {

			Vector3 pos = new Vector3(-33.0f, 0.0f, -4.0f);
			transform.position = pos;

			transform.Rotate(0.0f, 90.0f, 0.0f);

		} else if ( moveType == 2 ) {

			Vector3 pos = new Vector3(33.0f, 0.0f, -4.0f);
			transform.position = pos;

			transform.Rotate(0.0f, -90.0f, 0.0f);

		}

		StartCoroutine(Delay());
        
    }




    void Update() {

		if ( withdrawalFlag == false ) {

			if ( rotationFlag == false ) {
			
				transform.position += transform.forward * moveSpeed * Time.deltaTime;

			}

			if ( transform.position.z > 30.0f && enemyDeleteFlag == true ) {

			EnemyDelete();

			}

		} else if ( withdrawalFlag == true ) {

			transform.rotation = Quaternion.Euler (0.0f, 180.0f, 0.0f);

			transform.position -= transform.forward * moveSpeed * 10.0f * Time.deltaTime;

		}

    }




	void BlockGenerate() {

		Instantiate(blockPrefab, transform.position, transform.rotation);

		collisionFlag = false;

	}



    void EnemyDamage() {

		if ( destroyFlag == true ) {

			destroyFlag = false;
            
			Instantiate(explosionPrefab, transform.position, transform.rotation);
			Instantiate(explosionPrefab2, transform.position, transform.rotation);

			gameKeeper.SendMessage("EnemyD_Destroy");

            //スコアキーパーに死亡通知
            FindObjectOfType<ScoreKeeper>().SendEnemyDeath(gameObject, EnemyListEnum.Enemy_D, rawSocre);

            Destroy (this.gameObject);

		}

	}

    public void ScoreDisply(int score)
    {
		//表示処理
        var scoreTextobj = Instantiate(scoreTextPrefab, transform.position, transform.rotation);
        scoreTextobj.GetComponent<EnemyScoreView>().Show(score);

		//スコア加算
		FindObjectOfType<ScoreController>().OnScored(score);
    }

    public void ComboDisply(int combo)
    {
        var scoreTextobj = Instantiate(comboTextPrefab, transform.position + new Vector3(0, 0, -0.7f), transform.rotation);
        scoreTextobj.GetComponent<EnemyScoreView>().Show(combo);
    }




    void EnemyDelete() {

		gameKeeper.SendMessage("EnemyD_Delete");

		Destroy (this.gameObject);

	}




	void EnemyWithdrawal() {

		withdrawalFlag = true;

	}




	void OnTriggerEnter (Collider other) {

		if ( other.gameObject.tag == "EnemySeed" && collisionFlag == true ) {

			BlockGenerate();

		}

	}




	IEnumerator Delay() {

		yield return new WaitForSeconds( Random.Range(rotationWaitMin, rotationWaitMax) );

		rotationFlag = true;

		for(int i = 1; i < 19; ++i) {

			if ( moveType == 1 ) {

				transform.Rotate(0.0f, -5.0f, 0.0f);

			} else if ( moveType == 2 ) {

				transform.Rotate(0.0f, 5.0f, 0.0f);

			}

			yield return new WaitForSeconds(0.001f);

		}

		rotationFlag = false;

		collisionFlag = true;

		enemyDeleteFlag = true;

	}

}
