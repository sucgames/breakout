﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_A : MonoBehaviour {

	public GameObject gameKeeper;
	public GameObject explosionPrefab;
	public GameObject explosionPrefab2;
	public GameObject bulletPrefab;
	public GameObject scoreTextPrefab;
	public GameObject comboTextPrefab;

	public float moveSpeed;
	public float bulletWaitTime;
	public float bulletTimerMin;
	public float bulletTimerMax;
	public float bulletTimer;

	public int bulletType;
	public int specialBulletPercent;
	public int bulletNumber;
	public int bulletNumberMin;
	public int bulletNumberMax;

	public bool bulletFlag;
	public bool destroyFlag;
	public bool withdrawalFlag;

    //倒した時の素点
    [Header("倒した時の素点")]
    [SerializeField]
    private int rawSocre = 50;

 
    void Start() {

	    explosionPrefab.isStatic = true;
	    explosionPrefab2.isStatic = true;
	    bulletPrefab.isStatic = true;
	    scoreTextPrefab.isStatic = true;
        
		withdrawalFlag = false;

		destroyFlag = true;

		gameKeeper = GameObject.Find("GameKeeper");
		gameKeeper.SendMessage("EnemyA_Spawn");

		bulletFlag = true;

		bulletTimer = Random.Range(bulletTimerMin, bulletTimerMax);
        
    }




    void Update() {

		if ( withdrawalFlag == false ) {

			bulletTimer = bulletTimer - Time.deltaTime;

			if ( bulletTimer < 0.0f && bulletFlag == true ) {

				BulletOn();

			}

			transform.position -= transform.forward * moveSpeed * Time.deltaTime;

		} else if ( withdrawalFlag == true ) {

			transform.rotation = Quaternion.Euler (0.0f, 180.0f, 0.0f);

			transform.position -= transform.forward * moveSpeed * 10.0f * Time.deltaTime;

		}
        
    }




	void BulletOn() {
 
		StartCoroutine(Delay());
	}



    /// <summary>
    /// 敵が死んだときに実行される
    /// </summary>
	void EnemyDamage() {

		if ( destroyFlag == true ) {

			destroyFlag = false;

			
			Instantiate(explosionPrefab, transform.position, transform.rotation);
			Instantiate(explosionPrefab2, transform.position, transform.rotation);

			gameKeeper.SendMessage("EnemyA_Destroy");

            //スコアキーパーに死亡通知
            FindObjectOfType<ScoreKeeper>().SendEnemyDeath(gameObject,EnemyListEnum.Enemy_A,rawSocre);


            Destroy (this.gameObject);

		}

	}


    public void ScoreDisply(int score)
    {
		//表示処理
        var scoreTextobj = Instantiate(scoreTextPrefab, transform.position, transform.rotation);
        scoreTextobj.GetComponent<EnemyScoreView>().Show(score);

		//スコア加算
		FindObjectOfType<ScoreController>().OnScored(score);
    }

    public void ComboDisply(int combo)
    {
        var scoreTextobj = Instantiate(comboTextPrefab, transform.position + new Vector3(0,0,-0.7f), transform.rotation);
        scoreTextobj.GetComponent<EnemyScoreView>().Show(combo);
    }




	void EnemyDelete() {

		gameKeeper.SendMessage("EnemyA_Delete");

		Destroy (this.gameObject);

	}




	void EnemyWithdrawal() {

		withdrawalFlag = true;

	}




	IEnumerator Delay() {

		bulletType = Random.Range(0, specialBulletPercent);

		if ( bulletType == 0 ) {

			bulletNumber = bulletNumberMax;

		} else {

			bulletNumber = bulletNumberMin;

		}

		bulletFlag = false;

		for(int i = 0; i < bulletNumber; ++i) {

			yield return new WaitForSeconds(bulletWaitTime);

			Instantiate(bulletPrefab, transform.position, transform.rotation);
            gameKeeper.GetComponent<GameKeeper>().Bullet_Shot();

		}

		bulletTimer = Random.Range(bulletTimerMin, bulletTimerMax);

		bulletFlag = true;

	}

}
