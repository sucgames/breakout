﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    Enemy_A _enemyA = null;
    Enemy_B _enemyB = null;
    Enemy_C _enemyC = null;
    Enemy_D _enemyD = null;
    Enemy_E _enemyE = null;

    // 共通で使う変数
    public float MoveSpeed;
    public float MoveDownSpeed;
    public float MoveSideSpeed;
    public float WaitTime;
    public float WaitTimeCache;
    public int MoveType;

    // EnemyDだけで使える変数
    public float RotationWaitMin;
    public float RotationWaitMax;

    //EnemyEだけで使える変数
    public float Angle;
    public float RotateAngleMax;
    public float RotateAngleMin;
    public float MoveTime;
    public float MoveTimeMax;
    public float MoveTimeMin;
    public float WaitTimeMax;
    public float WaitTimeMin;

    // EnemyAだけで使える変数
    public float BulletWaitTime;
	public float BulletTimerMin;
	public float BulletTimerMax;
	public float BulletTimer;

    // Start is called before the first frame update
    void Start()
    {
        _enemyA = GetComponent<Enemy_A> ();
        _enemyB = GetComponent<Enemy_B> ();
        _enemyC = GetComponent<Enemy_C> ();
        _enemyD = GetComponent<Enemy_D> ();
        _enemyE = GetComponent<Enemy_E> ();

        if(_enemyA != null) EnemyA_Init();
        if(_enemyB != null) EnemyB_Init();
        if(_enemyC != null) EnemyC_Init();
        if(_enemyD != null) EnemyD_Init();
        if(_enemyE != null) EnemyE_Init();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // 移動スピードなどの初期設定を行います
    void EnemyA_Init()
    {
        _enemyA.moveSpeed = MoveSpeed;
        _enemyA.bulletWaitTime = BulletWaitTime;
	    _enemyA.bulletTimerMin = BulletTimerMin;
	    _enemyA.bulletTimerMax = BulletTimerMax;
	    _enemyA.bulletTimer = BulletTimer;
    }

    void EnemyB_Init()
    {
        _enemyB.moveDownSpeed = MoveDownSpeed;
	    _enemyB.moveSideSpeed = MoveSideSpeed;
	    _enemyB.waitTime = WaitTime;
	    _enemyB.waitTimeCache = WaitTimeCache;
    }

    void EnemyC_Init()
    {
        _enemyC.moveDownSpeed = MoveDownSpeed;
        _enemyC.moveSideSpeed = MoveSideSpeed;
	    _enemyC.moveType = MoveType;
	    
    }

    void EnemyD_Init()
    {
        _enemyD.moveSpeed = MoveSpeed;
        _enemyD.rotationWaitMin = RotationWaitMax;
	    _enemyD.rotationWaitMax = RotationWaitMin;
	    _enemyD.moveType = MoveType;
    }

    void EnemyE_Init()
    {
        _enemyE.moveSpeed = MoveSpeed;

        _enemyE.angle = Angle;
        _enemyE.rotateAngleMin = RotateAngleMin;
	    _enemyE.rotateAngleMax = RotateAngleMax;

        _enemyE.moveTime = MoveTime;
        _enemyE.moveTimeMin = MoveTimeMin;
        _enemyE.moveTimeMax = MoveTimeMax;

        _enemyE.waitTime = WaitTime;
        _enemyE.waitTimeMax = WaitTimeMax;
        _enemyE.waitTimeMin = WaitTimeMin;

	    _enemyE.moveType = MoveType;
    }

    // Update内でどのような動きをするかを設定します
    void EnemyBrain_A()
    {

    }

    void EnemyBrain_B()
    {
        
    }

    void EnemyBrain_C()
    {
        
    }

    void EnemyBrain_D()
    {
        
    }

    void EnemyBrain_E()
    {
        
    }
}
