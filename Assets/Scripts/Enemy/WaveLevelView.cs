using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// WAVE 000 を表示するView
class WaveLevelView : MonoBehaviour
{
    private List<GameObject> _numerics = new List<GameObject>();
    public float Margin;
    public float Kerning;

    public void Show(int level)
    {
        StartCoroutine(Write(new NumericTypography(Resources.LoadAll<GameObject>("wave/numerics")).MapToGameObject(level)));
    }

    // 数字を画面に書き込む
    private IEnumerator Write(List<GameObject> NumericSlot)
    {
        foreach (GameObject num in _numerics)
        {
            Destroy(num);
        }
        for (int i = 0; i < NumericSlot.Count; i++)
        {
            var Numeric = Instantiate(NumericSlot[i]);
            _numerics.Add(Numeric);
            Numeric.GetComponent<Transform>().position = this.GetComponent<Transform>().position + new Vector3(Margin + Kerning * i, 0, 0); // x軸方向に 親(Score)の初期位置 + カーニング * 桁 分ずらしている
        }
        yield return null;
    }
}