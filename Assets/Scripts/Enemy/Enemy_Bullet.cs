﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Bullet : MonoBehaviour {

	public GameObject explosionPrefab;

	public float moveSpeed;

 
    void Start() {
        
    }




    void Update() {

		transform.position -= transform.forward * moveSpeed * Time.deltaTime;
        
    }




	void BallHit() {

		Instantiate(explosionPrefab, transform.position, transform.rotation);

		Destroy (this.gameObject);

	}

}
