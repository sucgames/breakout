﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_E : MonoBehaviour {

	public GameObject gameKeeper;
	public GameObject explosionPrefab;
	public GameObject explosionPrefab2;
	public GameObject scoreTextPrefab;
    public GameObject comboTextPrefab;

    public float moveSpeed;

	public float angle;
	public float rotateAngleMin;
	public float rotateAngleMax;

	public float moveTime;
	public float moveTimeMin;
	public float moveTimeMax;

	public float waitTime;
	public float waitTimeMin;
	public float waitTimeMax;

	public float firstMove;

	public int moveType;

	public bool moveFlag;
	public bool downFlag;
	public bool destroyFlag;
	public bool withdrawalFlag;

    //倒した時の素点
    [Header("倒した時の素点")]
    [SerializeField]
    private int rawSocre = 3000;

    void Start() {

	    explosionPrefab.isStatic = true;
	    explosionPrefab2.isStatic = true;
	    scoreTextPrefab.isStatic = true;

		withdrawalFlag = false;

		destroyFlag = true;

		firstMove = Random.Range(15.0f, 25.0f);

		gameKeeper = GameObject.Find("GameKeeper");
		gameKeeper.SendMessage("EnemyE_Spawn");

		downFlag = false;

		StartCoroutine(Delay());
	       
    }




    void Update() {

		if ( withdrawalFlag == false ) {

			if ( transform.position.z > firstMove ) {

				transform.position -= transform.forward * moveSpeed * Time.deltaTime * 2;

				moveType = 0;

			}


			if ( moveFlag == true && transform.position.x >= -29.0f && transform.position.x <= 29.0f && transform.position.z <= 26.0f && transform.position.z >= -6.0f) {
			
				transform.position -= transform.forward * moveSpeed * Time.deltaTime;

			}

		} else if ( withdrawalFlag == true ) {

			transform.rotation = Quaternion.Euler (0.0f, 180.0f, 0.0f);

			transform.position -= transform.forward * moveSpeed * 20.0f * Time.deltaTime;

		}
        
    }




	void EnemyDamage() {

		if ( destroyFlag == true ) {

			destroyFlag = false;
            
			Instantiate(explosionPrefab, transform.position, transform.rotation);
			Instantiate(explosionPrefab2, transform.position, transform.rotation);

			downFlag = true;

			gameKeeper.SendMessage("EnemyE_Destroy");

            //スコアキーパーに死亡通知
            FindObjectOfType<ScoreKeeper>().SendEnemyDeath(gameObject, EnemyListEnum.Enemy_E, rawSocre);

            Destroy (this.gameObject);

		}

	}


    public void ScoreDisply(int score)
    {
		//表示処理
        var scoreTextobj = Instantiate(scoreTextPrefab, transform.position, transform.rotation);
        scoreTextobj.GetComponent<EnemyScoreView>().Show(score);

		//スコア加算
		FindObjectOfType<ScoreController>().OnScored(score);
    }

    public void ComboDisply(int combo)
    {
        var scoreTextobj = Instantiate(comboTextPrefab, transform.position + new Vector3(0, 0, -0.7f), transform.rotation);
        scoreTextobj.GetComponent<EnemyScoreView>().Show(combo);
    }


    void EnemyDelete() {

		gameKeeper.SendMessage("EnemyE_Delete");

		Destroy (this.gameObject);

	}




	void EnemyWithdrawal() {

		withdrawalFlag = true;

	}




	IEnumerator Delay() {

		while ( downFlag == false ) {

			moveFlag = false;

			moveType = Random.Range(1, 3);

			angle = Random.Range(rotateAngleMin, rotateAngleMax);

			for(int i = 1; i < angle; ++i) {

				if ( moveType == 1 ) {

					transform.Rotate(0.0f, -1.0f, 0.0f);

				} else if ( moveType == 2 ) {

					transform.Rotate(0.0f, 1.0f, 0.0f);

				}

				yield return new WaitForSeconds(0.05f);

			}

			waitTime = Random.Range(waitTimeMin, waitTimeMax);

			yield return new WaitForSeconds(waitTime);

			moveTime = Random.Range(moveTimeMin, moveTimeMax);

			moveFlag = true;

			yield return new WaitForSeconds(moveTime);

		}

	}

}
