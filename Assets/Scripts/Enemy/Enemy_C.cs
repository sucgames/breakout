﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_C : MonoBehaviour {

	public GameObject gameKeeper;
	public GameObject explosionPrefab;
	public GameObject explosionPrefab2;
	public GameObject scoreTextPrefab;
    public GameObject comboTextPrefab;

    public float moveDownSpeed;
	public float moveSideSpeed;

	public int moveType;

	public bool destroyFlag;
	public bool withdrawalFlag;

    //倒した時の素点
    [Header("倒した時の素点")]
    [SerializeField]
    private int rawSocre = 300;


    void Start() {

	    explosionPrefab.isStatic = true;
	    explosionPrefab2.isStatic = true;
	    scoreTextPrefab.isStatic = true;
        
		withdrawalFlag = false;

		destroyFlag = true;

		gameKeeper = GameObject.Find("GameKeeper");
		gameKeeper.SendMessage("EnemyC_Spawn");

		moveType = Random.Range(1, 3);

    }




    void Update() {

		if ( withdrawalFlag == false ) {

			transform.position -= transform.forward * moveDownSpeed * Time.deltaTime;

			if ( moveType == 1 ) {
			
				transform.position -= transform.right * moveSideSpeed * Time.deltaTime;

			} else if ( moveType == 2 ) {

				transform.position += transform.right * moveSideSpeed * Time.deltaTime;

			}


			if ( transform.position.x <= -31.0f ) {

				moveType = 2;

			} else if ( transform.position.x >= 31.0f ) {

				moveType = 1;

			}

		} else if ( withdrawalFlag == true ) {

			transform.rotation = Quaternion.Euler (0.0f, 180.0f, 0.0f);

			transform.position -= transform.forward * moveDownSpeed * 5.0f * Time.deltaTime;

		}

    }



    void EnemyDamage() {

		if ( destroyFlag == true ) {

			destroyFlag = false;
            
			Instantiate(explosionPrefab, transform.position, transform.rotation);
			Instantiate(explosionPrefab2, transform.position, transform.rotation);

			gameKeeper.SendMessage("EnemyC_Destroy");

            //スコアキーパーに死亡通知
            FindObjectOfType<ScoreKeeper>().SendEnemyDeath(gameObject, EnemyListEnum.Enemy_C, rawSocre);

            Destroy (this.gameObject);

		}

	}

    public void ScoreDisply(int score)
    {
		//表示処理
        var scoreTextobj = Instantiate(scoreTextPrefab, transform.position, transform.rotation);
        scoreTextobj.GetComponent<EnemyScoreView>().Show(score);

		//スコア加算
		FindObjectOfType<ScoreController>().OnScored(score);
    }

    public void ComboDisply(int combo)
    {
        var scoreTextobj = Instantiate(comboTextPrefab, transform.position + new Vector3(0, 0, -0.7f), transform.rotation);
        scoreTextobj.GetComponent<EnemyScoreView>().Show(combo);
    }




    void EnemyWithdrawal() {

		withdrawalFlag = true;

	}




	void EnemyDelete() {

		gameKeeper.SendMessage("EnemyC_Delete");

		Destroy (this.gameObject);

	}

}
