using UnityEngine;
[CreateAssetMenu(menuName = "Score/ComboCurve")]
public class ComboCurveConfig: ScriptableObject
{
    public string EnemyScoreName;
    // コンボ後に次の敵を倒してコンボが繋がるまでの制限時間
    public int ComboDuration = 1;
    // コンボ限界
    public int ComboLimit = 10;
    public float[] rates; // [1.0f,2.0f,3.0f,5.0f,7.0f]


    public float Rate(int Count)
    {
        return rates[Count];
    }
}