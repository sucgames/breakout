using System;
using UnityEngine;
public class WaveConfig : ScriptableObject
{
    public AudioClip bgm;
    public GameObject[] enemyObjects;
    //敵A~Eの出現頻度・出現パターン
    //waveの終了条件（撤退あるか？何秒後に撤退するか）
    //モンスターハウスがあるか
    //ボーナスステージか
    public float spawnTimerMin;
    public float spawnTimerMax;

    public float waveTime;

    public float posX;
    public float posZ;

    public int targetObjectType;
    public int targetObjectTypeMin;
    public int targetObjectTypeMax;

    public int enemyATypeMin;
    public int enemyATypeMax;

    public int enemyBTypeMin;
    public int enemyBTypeMax;

    public int enemyCTypeMin;
    public int enemyCTypeMax;

    public int enemyDTypeMin;
    public int enemyDTypeMax;

    public int enemyETypeMin;
    public int enemyETypeMax;

    public int multiballTypeMin;
    public int multiballTypeMax;

    public bool randomSpawnFlag;


    public GameObject SpawnEnemy(int targetObjectType)
    {
        if (targetObjectType >= enemyATypeMin && targetObjectType <= enemyATypeMax)
        {

            return enemyObjects[(int)EnemyType.A];

        }
        else if (targetObjectType >= enemyBTypeMin && targetObjectType <= enemyBTypeMax)
        {

            return enemyObjects[(int)EnemyType.B];

        }
        else if (targetObjectType >= enemyCTypeMin && targetObjectType <= enemyCTypeMax)
        {

            return enemyObjects[(int)EnemyType.C];

        }
        else if (targetObjectType >= enemyDTypeMin && targetObjectType <= enemyDTypeMax)
        {

            return enemyObjects[(int)EnemyType.D];

        }
        else if (targetObjectType >= enemyETypeMin && targetObjectType <= enemyETypeMax)
        {

            return enemyObjects[(int)EnemyType.E];

        }
        else if (targetObjectType >= multiballTypeMin && targetObjectType <= multiballTypeMax)
        {
            return null;
        }
        throw new ArgumentException("不正なEnemyTypeが渡された");
    }
}