﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialEnemyGenerator : MonoBehaviour {

	public GameObject gameKeeper;
	public GameObject multiballGenerator;
	public GameObject waveText;
	public GameObject spawnEffect;
    [Header("Wave数を入力")]
    [SerializeField]
    int waveCount = 1;
    public GameObject[] enemyObject;


	public float randomPositionXMin;
	public float randomPositionXMax;
	public float randomPositionZMin;
	public float randomPositionZMax;

	public float spawnTimer;
	public float spawnTimerMin;
	public float spawnTimerMax;
	public float spawnTimerCache;

	public float waveTime;

	public float posX;
	public float posZ;

	public int targetObjectType;
	public int targetObjectTypeMin;
	public int targetObjectTypeMax;

	public int enemyATypeMin;
	public int enemyATypeMax;

	public int enemyBTypeMin;
	public int enemyBTypeMax;

	public int enemyCTypeMin;
	public int enemyCTypeMax;

	public int enemyDTypeMin;
	public int enemyDTypeMax;

	public int enemyETypeMin;
	public int enemyETypeMax;

	public int multiballTypeMin;
	public int multiballTypeMax;

	public bool randomSpawnFlag;


    void Start() {

		var waveTextobj = Instantiate(waveText, transform.position, transform.rotation);
        waveTextobj.GetComponent<WaveView>().Updatewave(waveCount);

		gameKeeper = GameObject.Find("GameKeeper");
		gameKeeper.SendMessage("WaveStart");

		multiballGenerator = GameObject.Find("MultiballGenerator");

		spawnTimer = Random.Range(spawnTimerMin, spawnTimerMax);

		spawnTimerCache = spawnTimer;

		StartCoroutine(Delay());
        
    }




    void Update() {

		if ( randomSpawnFlag == true ) {
			
			spawnTimerCache = spawnTimerCache - Time.deltaTime;

			if ( spawnTimerCache < 0.0f ) {

				EnemyGenerate();

			}

		}
        
    }




	void EnemyGenerate() {

		targetObjectType = Random.Range(targetObjectTypeMin, targetObjectTypeMax);

		posX = Random.Range(randomPositionXMin, randomPositionXMax);
		posZ = Random.Range(randomPositionZMin, randomPositionZMax);
		transform.localPosition = new Vector3(posX, 0.0f, posZ);


		if ( targetObjectType >= enemyATypeMin && targetObjectType <= enemyATypeMax ) {
			
			Instantiate(enemyObject[0], transform.position, transform.rotation);

		} else if ( targetObjectType >= enemyBTypeMin && targetObjectType <= enemyBTypeMax ) {

			Instantiate(enemyObject[1], transform.position, transform.rotation);

		} else if ( targetObjectType >= enemyCTypeMin && targetObjectType <= enemyCTypeMax ) {

			Instantiate(enemyObject[2], transform.position, transform.rotation);

		} else if ( targetObjectType >= enemyDTypeMin && targetObjectType <= enemyDTypeMax ) {

			Instantiate(enemyObject[3], transform.position, transform.rotation);

		} else if ( targetObjectType >= enemyETypeMin && targetObjectType <= enemyETypeMax ) {

			Instantiate(enemyObject[4], transform.position, transform.rotation);

		} else if ( targetObjectType >= multiballTypeMin && targetObjectType <= multiballTypeMax ) {

			multiballGenerator.SendMessage("BallGenerate");

		}


		Instantiate(spawnEffect, transform.position, transform.rotation);


		spawnTimer = Random.Range(spawnTimerMin, spawnTimerMax);

		spawnTimerCache = spawnTimer;

	}




	void WaveClear() {

		Destroy (this.gameObject);

	}




	IEnumerator Delay() {

		randomSpawnFlag = true;

		yield return new WaitForSeconds(waveTime);

		randomSpawnFlag = false;

		gameKeeper.SendMessage("WaveDone");

	}

}
