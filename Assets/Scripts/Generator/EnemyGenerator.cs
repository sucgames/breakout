﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour {

	public GameObject gameKeeper;
	public GameObject multiballGenerator;
	public GameObject[] enemyObject;

	public float spawnTimer;
	public float spawnTimerMin;
	public float spawnTimerMax;
	public float spawnTimerCache;

	public float waveTime;

	public float posX;
	public float posZ;

	public int targetObjectType;
	public int targetObjectTypeMin;
	public int targetObjectTypeMax;

	public int enemyATypeMin;
	public int enemyATypeMax;

	public int enemyBTypeMin;
	public int enemyBTypeMax;

	public int enemyCTypeMin;
	public int enemyCTypeMax;

	public int enemyDTypeMin;
	public int enemyDTypeMax;

	public int enemyETypeMin;
	public int enemyETypeMax;

	public int multiballTypeMin;
	public int multiballTypeMax;

	public bool randomSpawnFlag;


    void Start() {

		gameKeeper = GameObject.Find("GameKeeper");
		gameKeeper.SendMessage("WaveStart");

		multiballGenerator = GameObject.Find("MultiballGenerator");

		spawnTimer = Random.Range(spawnTimerMin, spawnTimerMax);

		spawnTimerCache = spawnTimer;

		StartCoroutine(Delay());

        for (var i = 0; i < enemyObject.Length; i++) {
            enemyObject[i].isStatic = true;
        }
        
    }




    void Update() {

		if ( randomSpawnFlag == true ) {
			
			spawnTimerCache = spawnTimerCache - Time.deltaTime;

			if ( spawnTimerCache < 0.0f ) {

				EnemyGenerate();

			}

		}
        
    }




	void EnemyGenerate() {

		targetObjectType = Random.Range(targetObjectTypeMin, targetObjectTypeMax);

		posX = Random.Range(-30.0f, 30.0f);
		posZ = Random.Range(30.0f, 45.0f);
		transform.localPosition = new Vector3(posX, 0.0f, posZ);


		if ( targetObjectType >= enemyATypeMin && targetObjectType <= enemyATypeMax ) {
			
			Instantiate(enemyObject[0], transform.position, transform.rotation);

		} else if ( targetObjectType >= enemyBTypeMin && targetObjectType <= enemyBTypeMax ) {

			Instantiate(enemyObject[1], transform.position, transform.rotation);

		} else if ( targetObjectType >= enemyCTypeMin && targetObjectType <= enemyCTypeMax ) {

			Instantiate(enemyObject[2], transform.position, transform.rotation);

		} else if ( targetObjectType >= enemyDTypeMin && targetObjectType <= enemyDTypeMax ) {

			Instantiate(enemyObject[3], transform.position, transform.rotation);

		} else if ( targetObjectType >= enemyETypeMin && targetObjectType <= enemyETypeMax ) {

			Instantiate(enemyObject[4], transform.position, transform.rotation);

		} else if ( targetObjectType >= multiballTypeMin && targetObjectType <= multiballTypeMax ) {

			multiballGenerator.SendMessage("BallGenerate");

		}


		spawnTimer = Random.Range(spawnTimerMin, spawnTimerMax);

		spawnTimerCache = spawnTimer;

	}




	IEnumerator Delay() {

		randomSpawnFlag = true;

		yield return new WaitForSeconds(waveTime);

		randomSpawnFlag = false;

		gameKeeper.SendMessage("WaveDone");

	}

}
