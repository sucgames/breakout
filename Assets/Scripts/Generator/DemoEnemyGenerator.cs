﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoEnemyGenerator : MonoBehaviour {

	public GameObject enemyObject;
	public GameObject spawnEffect;

	public float randomPositionXMin;
	public float randomPositionXMax;
	public float randomPositionZMin;
	public float randomPositionZMax;

	public float spawnTimer;
	public float spawnTimerMin;
	public float spawnTimerMax;
	public float spawnTimerCache;

	public float posX;
	public float posZ;


    void Start() {

		spawnTimer = Random.Range(spawnTimerMin, spawnTimerMax);

		spawnTimerCache = spawnTimer;
        
    }




    void Update() {

		spawnTimerCache = spawnTimerCache - Time.deltaTime;

		if ( spawnTimerCache < 0.0f ) {

			EnemyGenerate();

		}
        
    }




	void EnemyGenerate() {

		posX = Random.Range(randomPositionXMin, randomPositionXMax);
		posZ = Random.Range(randomPositionZMin, randomPositionZMax);
		transform.localPosition = new Vector3(posX, 0.0f, posZ);


		Instantiate(enemyObject, transform.position, transform.rotation);

		Instantiate(spawnEffect, transform.position, transform.rotation);


		spawnTimer = Random.Range(spawnTimerMin, spawnTimerMax);

		spawnTimerCache = spawnTimer;

	}

}
