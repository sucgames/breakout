﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Collision : MonoBehaviour {

	public GameObject player;


	void Start() {

	}




	void Update() {

	}




	void OnCollisionEnter(Collision collision) {

		if ( collision.gameObject.tag == "Enemy" ) {

			player.SendMessage("PlayerDamage");

		}

		if ( collision.gameObject.tag == "Bullet" ) {

			collision.gameObject.SendMessage("BallHit");
			player.SendMessage("PlayerDamage");

		}

	}

}
