﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.SliderJoint.Scripts;

public class Player2_SliderJoint : MonoBehaviour {

	SliderJoint joint;

	public float verticalKeyInput;
	public float verticalKeyInput2;

	public bool leftPadFlag;
	public bool rightPadFlag;

	public bool analogPadFlag;


	void Start() {

		analogPadFlag = false;

		joint = gameObject.GetComponent<SliderJoint>();

	}




	void Update() {

		verticalKeyInput = Input.GetAxis("VerticalAnalogPad2");
		if ( verticalKeyInput < -0.5f ) {

			leftPadFlag = true;

			analogPadFlag = true;

			GimmickOn();

		} else if ( verticalKeyInput > -0.5f && analogPadFlag == true ) {

			leftPadFlag = false;

			GimmickOff();

		}


		verticalKeyInput2 = Input.GetAxis("VerticalExPad2");
		if ( verticalKeyInput2 < -0.5f ) {

			rightPadFlag = true;

			analogPadFlag = true;

			GimmickOn();

		} else if ( verticalKeyInput2 > -0.5f && analogPadFlag == true ) {

			rightPadFlag = false;

			GimmickOff();

		}








		if ( Input.GetButtonDown ("SubmitEX") ) {

			leftPadFlag = true;

			rightPadFlag = true;

			analogPadFlag = false;

			GimmickOn();

		} else if ( Input.GetButtonUp ("SubmitEX") ) {

			leftPadFlag = false;

			rightPadFlag = false;

			analogPadFlag = true;

			GimmickOff();

		}

	}




	public void GimmickOn() {

		if ( leftPadFlag == true && rightPadFlag == true ) {

			joint.UseMotor = true;

		}

	}




	public void GimmickOff() {

		if ( leftPadFlag == false || rightPadFlag == false ) {

			joint.UseMotor = false;

		}

	}

}
