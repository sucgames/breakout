﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2 : MonoBehaviour {

	public GameObject gameKeeper;
	public GameObject explosionPrefab;
	public GameObject levelUpPrefab;

	public ParticleSystem bulletTimeEffect;

	public float targetVelocity;
	public float targetVelocityCache;
	public float horizontalKeyInput;
	public float horizontalKeyInput2;
	public float horizontalKeyInput3;

	public float bulletTime;
	public float bulletTimeScale;

	public bool analogPadFlag;

	public bool leftMoveFlag;
	public bool rightMoveFlag;


	void Start() {

		bulletTimeEffect.Stop (true, ParticleSystemStopBehavior.StopEmitting);

		gameKeeper = GameObject.Find("GameKeeper");
		gameKeeper.SendMessage("Player2_Spawn");

		analogPadFlag = false;

	}




	void Update() {

		if ( Input.GetKey ("a") ) {

			analogPadFlag = false;

			leftMoveFlag = true;

		} else if ( Input.GetKeyUp ("a") ) {

			analogPadFlag = true;

			leftMoveFlag = false;

		}


		if ( Input.GetKey ("d") ) {

			analogPadFlag = false;

			rightMoveFlag = true;

		} else if ( Input.GetKeyUp ("d") ) {

			analogPadFlag = true;

			rightMoveFlag = false;

		}




		if ( Input.GetButtonDown ("ExButton2") ) {

			StartCoroutine(Delay());

		}




		horizontalKeyInput = Input.GetAxis("HorizontalAnalogPad2");
		if ( horizontalKeyInput < -0.5f ) {

			analogPadFlag = true;

			GimmickLeftOn();

		} else if ( horizontalKeyInput > 0.5f ) {

			analogPadFlag = true;

			GimmickRightOn();

		} else if ( horizontalKeyInput >= -0.5f && horizontalKeyInput <= 0.5f && analogPadFlag == true ) {

			analogPadFlag = false;

			GimmickOff();

		}




		horizontalKeyInput2 = Input.GetAxis("HorizontalExPad2");
		if ( horizontalKeyInput2 < -0.5f ) {

			analogPadFlag = true;

			GimmickLeftOn();

		} else if ( horizontalKeyInput2 > 0.5f ) {

			analogPadFlag = true;

			GimmickRightOn();

		}




		horizontalKeyInput3 = Input.GetAxis("HorizontalDPad2");
		if ( horizontalKeyInput3 < -0.5f ) {

			analogPadFlag = true;

			GimmickLeftOn();

		} else if ( horizontalKeyInput3 > 0.5f ) {

			analogPadFlag = true;

			GimmickRightOn();

		}




		if ( leftMoveFlag == true ) {

			if ( transform.position.x > -28.6f ) {

				if ( analogPadFlag == true ) {

					targetVelocityCache = (horizontalKeyInput + horizontalKeyInput2 + horizontalKeyInput3) * -targetVelocity;

				} else {

					targetVelocityCache = targetVelocity * 2;

				}

				transform.position -= transform.right * targetVelocityCache * Time.deltaTime;

			} else if ( transform.position.x < -28.6f ) {

				transform.localPosition = new Vector3(-28.6f, 0.0f, 0.0f);

			}

		}




		if ( rightMoveFlag == true ) {

			if ( transform.position.x < 28.6f ) {

				if ( analogPadFlag == true ) {

					targetVelocityCache = (horizontalKeyInput + horizontalKeyInput2 + horizontalKeyInput3) * targetVelocity;

				} else {

					targetVelocityCache = targetVelocity * 2;

				}

				transform.position += transform.right * targetVelocityCache * Time.deltaTime;

			} else if ( transform.position.x > 28.6f ) {

				transform.localPosition = new Vector3(28.6f, 0.0f, 0.0f);

			}

		}

	}




	void GimmickLeftOn() {

		leftMoveFlag = true;

	}

	void GimmickLeftOff() {

		leftMoveFlag = false;

	}


	public void TouchLeftOn() {

		analogPadFlag = false;

		leftMoveFlag = true;

	}

	public void TouchLeftOff() {

		analogPadFlag = true;

		leftMoveFlag = false;

	}




	void GimmickRightOn() {

		rightMoveFlag = true;

	}

	void GimmickRightOff() {

		rightMoveFlag = false;

	}


	public void TouchRightOn() {

		analogPadFlag = false;

		rightMoveFlag = true;

	}

	public void TouchRightOff() {

		analogPadFlag = true;

		rightMoveFlag = false;

	}




	public void GimmickOff() {

		targetVelocityCache = 0.0f;

		leftMoveFlag = false;

		rightMoveFlag = false;

	}




	void PlayerDamage() {

		gameKeeper.SendMessage("Player2_Destroy");

		Instantiate(explosionPrefab, transform.position, transform.rotation);

		Destroy (this.gameObject);

	}




	void LevelUp() {

		Instantiate(levelUpPrefab, transform.position, transform.rotation);

	}




	IEnumerator Delay() {

		bulletTimeEffect.Play (true);

		Time.timeScale = bulletTimeScale;

		yield return new WaitForSeconds(bulletTime);

		Time.timeScale = 1.0f;

		bulletTimeEffect.Stop (true, ParticleSystemStopBehavior.StopEmitting);

	}

}
