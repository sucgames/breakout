﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player1_HingeJoint : MonoBehaviour {

	HingeJoint joint;
	JointMotor jointmotor;
	JointLimits limits;

	public float verticalKeyInput;
	public float verticalKeyInput2;

	public float targetVelocity;
	public float hingeLimits;
	public float trigerInput;

	public int leftPadFlag;
	public int rightPadFlag;

	public bool analogPadFlag;


	void Start() {

		analogPadFlag = false;

		joint = gameObject.GetComponent<HingeJoint>();

		jointmotor = joint.motor;

		limits = joint.limits;

		limits.min = -hingeLimits;
		limits.max = hingeLimits;
		joint.limits = limits;
		joint.useLimits = true;

	}




	void Update() {

		verticalKeyInput = Input.GetAxis("VerticalAnalogPad1");
		if ( verticalKeyInput < -0.5f ) {

			leftPadFlag = 1;

			analogPadFlag = true;

		} else if ( verticalKeyInput > 0.5f ) {

			leftPadFlag = -1;

			analogPadFlag = true;

		} else if ( verticalKeyInput > -0.5f && verticalKeyInput < 0.5f ) {

			leftPadFlag = 0;

		}




		verticalKeyInput2 = Input.GetAxis("VerticalExPad1");
		if ( verticalKeyInput2 < -0.5f ) {

			rightPadFlag = 1;

			analogPadFlag = true;

		} else if ( verticalKeyInput2 > 0.5f ) {

			rightPadFlag = -1;

			analogPadFlag = true;

		} else if ( verticalKeyInput2 > -0.5f && verticalKeyInput2 < 0.5f ) {

			rightPadFlag = 0;

		}




		if ( leftPadFlag == -1 && rightPadFlag == 1 ) {

			GimmickLeftOn();

		} else if ( leftPadFlag == 1 && rightPadFlag == -1 ) {

			GimmickRightOn();

		} else if ( leftPadFlag == 0 && rightPadFlag == 0 && analogPadFlag == true ) {

			GimmickOff();

		}






		if ( Input.GetKeyDown (KeyCode.LeftArrow) ) {

			analogPadFlag = false;

			GimmickLeftOn();

		} else if ( Input.GetKeyUp (KeyCode.LeftArrow) ) {

			analogPadFlag = true;

			GimmickOff();

		}


		if ( Input.GetKeyDown (KeyCode.RightArrow) ) {

			analogPadFlag = false;

			GimmickRightOn();

		} else if ( Input.GetKeyUp (KeyCode.RightArrow) ) {

			analogPadFlag = true;

			GimmickOff();

		}

	}




	public void GimmickLeftOn() {

		joint.useMotor = true;

		jointmotor.targetVelocity = -targetVelocity;
		joint.motor = jointmotor;

	}

	public void GimmickRightOn() {

		joint.useMotor = true;

		jointmotor.targetVelocity = targetVelocity;
		joint.motor = jointmotor;

	}




	public void GimmickOff() {

		joint.useMotor = false;

	}

}
