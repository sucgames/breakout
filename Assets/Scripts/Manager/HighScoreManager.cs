﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScoreManager : MonoBehaviour
{

    // TODO: ScoreTextViewに置き換える
    ScoreView scoreview;

    // Start is called before the first frame update
    void Start()
    {
        scoreview = gameObject.AddComponent<ScoreView>();
        scoreview.Margin = 12.5f;
        scoreview.UpdateScore(PlayerPrefs.GetInt(PlayerPrefsKey.HighScoreKey,0));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
