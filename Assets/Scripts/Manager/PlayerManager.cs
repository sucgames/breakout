﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

	public GameObject player1P;
	public GameObject player2P;
	public GameObject player3P;
	public GameObject player4P;

	public int gamePlayerNumber;

	private string gamePlayerMenuKey = "gamePlayerMenu";


    void Start() {

		gamePlayerNumber = PlayerPrefs.GetInt (gamePlayerMenuKey, 1);

		FirstCheck();
        
    }




    void Update() {
        
    }




	void FirstCheck() {

		if ( gamePlayerNumber == 1 ) {

			player1P.gameObject.SetActive(true);

		} else if ( gamePlayerNumber == 2 ) {

			player2P.gameObject.SetActive(true);

		} else if ( gamePlayerNumber == 3) {

			player3P.gameObject.SetActive(true);

		} else if ( gamePlayerNumber == 4 ) {

			player4P.gameObject.SetActive(true);

		}

	}

}
