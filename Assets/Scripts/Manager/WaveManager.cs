﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour {

	public GameObject waveKeeper;
	public GameObject exWall;
	public GameObject withdrawal;

	public int nextWaveWaitTime;

	public int waveCount;


    void Start() {
        
    }




    void Update() {
        
    }




	void ExWall_Spawn() {

		exWall.gameObject.SetActive(true);

		if ( waveCount == 1 ) {

			exWall.SendMessage("EnduranceEternal");

		} else if ( waveCount == 2 ) {

			exWall.SendMessage("Endurance5");

		} else if ( waveCount == 3 ) {

			exWall.SendMessage("Endurance5");

		} else if ( waveCount >= 4 ) {

			exWall.SendMessage("Endurance5");

		}

	}




	void ExWall_Delete() {

		exWall.gameObject.SetActive(false);

	}




	void WaveStart() {

		waveCount = waveCount + 1;

		if ( waveCount > 1 ) {

			BroadcastMessage("WaveClear");

			Instantiate(withdrawal, transform.position, transform.rotation);

		}

		waveKeeper.SendMessage("WaveGenerate");

		ExWall_Spawn();

	}

}