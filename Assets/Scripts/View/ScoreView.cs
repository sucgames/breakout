using System.Collections;
using System.Collections.Generic;
using UnityEngine;
class ScoreView : MonoBehaviour {
    // 表示する数字のプレハブ群
    [SerializeField, Tooltip ("数字のプレハブを入れてね")]
    public GameObject[] Numerics = new GameObject[10];

    private List<GameObject> _scoreNumerics = new List<GameObject>();
    // スコア
    public int Score = 0;
    // 文字のアキ
    public float Kerning = 1f;
    public float Margin = 6;

    public void Awake()
    {
        Numerics = Resources.LoadAll<GameObject>("Prefab/Text/sozai/ScoreText_L/Numerics");
        for(int i = 0; i < Numerics.Length; i++)
        {
            Numerics[i].isStatic = true;
        }
    }
    // スコアを更新する
    public void UpdateScore (int score) {
        //Score += score;
        Score = score;
        StartCoroutine (Write (new NumericTypography (Numerics).MapToGameObject (Score)));
    }
    // 数字を画面に書き込む
    private IEnumerator Write (List<GameObject> NumericSlot) {
        foreach (GameObject num in _scoreNumerics) {
            Destroy(num);
        }
        for (int i = 0; i < NumericSlot.Count; i++) {
            var Numeric = Instantiate (NumericSlot[i]);
            _scoreNumerics.Add(Numeric);
            Numeric.GetComponent<Transform> ().position = GetComponent<Transform> ().position + new Vector3 (Margin + Kerning * i, 0, 0); // x軸方向に 親(Score)の初期位置 + カーニング * 桁 分ずらしている
        }
        yield return null;
    }
}