using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyScoreView : MonoBehaviour
{
    public GameObject[] Numerics;
    private List<GameObject> _scoreNumerics = new List<GameObject>();
    // 文字のアキ
    public float Kerning = 1f;
    public float Margin = 3;
    public void Show(int score)
    {
        StartCoroutine (Write (new NumericTypography (Numerics).MapToGameObject (score)));
        // NumericTypographyでscoreを描画
        // NumericTypographyでcomboを描画
        // comboを描画
    }

    // 数字を画面に書き込む
    private IEnumerator Write (List<GameObject> NumericSlot) {
        for (int i = 0; i < NumericSlot.Count; i++) {
            var Numeric = Instantiate (NumericSlot[i]);
            _scoreNumerics.Add(Numeric);
            Numeric.GetComponent<Transform> ().position = GetComponent<Transform> ().position + new Vector3 (Margin + Kerning * i, 0, 0); // x軸方向に 親(Score)の初期位置 + カーニング * 桁 分ずらしている
        }
        yield return null;
    }

    private void OnDestroy()
    {
        AllNumericsDestroy();
    }

    void AllNumericsDestroy()
    {
        for (int i = 0; i < _scoreNumerics.Count; i++)
        {
            Destroy(_scoreNumerics[i]);
        }
    }
}