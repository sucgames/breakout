using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyComboView : MonoBehaviour
{
    public GameObject[] Numerics;
    private List<GameObject> _numerics = new List<GameObject>();
    // 文字のアキ
    public float Kerning = 1f;
    public float Margin = 3;
    public void Show(int combo)
    {
        StartCoroutine (Write (new NumericTypography (Numerics).MapToGameObject (combo)));
    }

    // 数字を画面に書き込む
    private IEnumerator Write (List<GameObject> NumericSlot) {
        for (int i = 0; i < NumericSlot.Count; i++) {
            var Numeric = Instantiate (NumericSlot[i]);
            _numerics.Add(Numeric);
            Numeric.GetComponent<Transform> ().position = GetComponent<Transform> ().position + new Vector3 (Margin + Kerning * i, 0, 0); // x軸方向に 親(Score)の初期位置 + カーニング * 桁 分ずらしている
        }
        yield return null;
    }
}