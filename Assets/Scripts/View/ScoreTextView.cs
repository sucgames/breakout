﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ScoreTextView : MonoBehaviour
{
    private List<Transform> ScoreTextList;
    public int Score = 12345;
    void Awake()
    {
       ScoreTextList = transform.Find("ScoreTexts").transform.Cast<Transform>().ToList();
    }
    // 仕組み
    // 現在のスコアを非表示にして、新しいスコアを表示する
    // 000000
    // 001000 -> 00____
    // 012000 -> 0_____ -> 012000
    public void UpdateScore(int score){
        MapDigit(Score, ScoreTextList, false);
        MapDigit(score, ScoreTextList, true);

        // 桁上がり時の0をDeactivate
        int[] digits = score.ToString().Select(c => Convert.ToInt32(c.ToString())).ToArray();
        var carry = ScoreTextList[digits.Length - 1].transform;
        carry.Cast<Transform>().ToArray()[0].gameObject.SetActive(false);

        // キャッシュ
        Score = score;
    }

    private void MapDigit(int score, List<Transform> scoreTextList, bool isActive)
    {
        int[] digits = score.ToString().Select(c => Convert.ToInt32(c.ToString())).ToArray();

        for (int i = 0; i < digits.Length; i++)
        {
            // GameObjectの名前で検索していないため、プレハブ上の配置順が変わると壊れます
            var scoreNumber = scoreTextList[digits.Length - i - 1].transform;
            scoreNumber.Cast<Transform>().ToArray()[digits[i]].gameObject.SetActive(isActive);
        }
    }
}
