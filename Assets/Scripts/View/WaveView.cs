using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class WaveView : MonoBehaviour
{
    // 表示する数字のプレハブ群
    [SerializeField, Tooltip("数字のプレハブを入れてね")]
    public GameObject[] Numerics = new GameObject[10];

    private List<GameObject> _waveNumerics = new List<GameObject>();
    // ウェーブ
    public int wave = 0;
    // 文字のアキ
    public float Kerning = 1f;
    public float Margin = 6;

    public void Awake()
    {
        Numerics = Resources.LoadAll<GameObject>("Prefab/Text/sozai/WaveText/Move");
        for (int i = 0; i < Numerics.Length; i++)
        {
            Numerics[i].isStatic = true;
        }
    }
    // スコアを更新する
    public void Updatewave(int nowWave)
    {
        //wave += wave;
        wave = nowWave;
        StartCoroutine(Write(new NumericTypography(Numerics).MapToGameObject(nowWave)));
    }
    // 数字を画面に書き込む
    private IEnumerator Write(List<GameObject> NumericSlot)
    {
        foreach (GameObject num in _waveNumerics)
        {
            Destroy(num);
        }
        for (int i = 0; i < NumericSlot.Count; i++)
        {
            var Numeric = Instantiate(NumericSlot[i]);
            _waveNumerics.Add(Numeric);
            Numeric.GetComponent<Transform>().position = GetComponent<Transform>().position + new Vector3(Margin + Kerning * i, 0, 0); // x軸方向に 親(wave)の初期位置 + カーニング * 桁 分ずらしている
        }
        yield return null;
    }
}